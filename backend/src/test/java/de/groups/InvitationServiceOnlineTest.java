package de.groups;

import de.groups.models.Group;
import de.groups.models.GroupMember;
import de.groups.models.Invitation;
import de.groups.models.User;
import de.groups.services.GroupMemberService;
import de.groups.services.GroupService;
import de.groups.services.InvitationService;
import de.groups.services.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("online-test")
@ContextConfiguration(classes = {OnlineTestConfig.class}, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class InvitationServiceOnlineTest {

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private GroupMemberService groupMemberService;

    private Timestamp timestamp;

    @BeforeEach
    public void setUp() throws Exception {
        Assert.assertNotNull(invitationService);
        Assert.assertNotNull(groupService);
        Assert.assertNotNull(groupMemberService);
        Assert.assertNotNull(userService);

        timestamp = new Timestamp(new Date().getTime());

        invitationService.deleteAll();
        groupService.deleteAll();
        userService.deleteAll();
        groupMemberService.deleteAll();
    }

    @AfterEach
    public void tearDown() throws Exception {
        timestamp = null;

        invitationService.deleteAll();
        groupService.deleteAll();
        userService.deleteAll();
        groupMemberService.deleteAll();
    }

    @Test
    public void testSaveGroupEntryRequestSuccess() {
        GroupMember groupMember = groupMemberService.save(new GroupMember());

        User tmpUser = new User("Test User");

        List<GroupMember> groupMembersOfUser = tmpUser.getGroupRepresentations();
        groupMembersOfUser.add(groupMember);
        tmpUser.setGroupRepresentations(groupMembersOfUser);

        User user = userService.save(tmpUser);

        Group tmpGroup = new Group("Test Gruppe", timestamp, "Test Kategorie", "Test Beschreibung", user);

        List<GroupMember> groupMembersOfGroup = tmpGroup.getMembers();
        groupMembersOfGroup.add(groupMember);
        tmpGroup.setMembers(groupMembersOfGroup);

        Group group = groupService.save(tmpGroup);

        Invitation invitation = new Invitation();

        invitation.setCreatedAt(timestamp);
        invitation.setRelatedGroup(group);
        invitation.setRelatedUser(user);
        invitation.setIssuer(groupMember);

        invitationService.save(invitation);

        List<Invitation> invitationList = invitationService.findAll();
        Assert.assertNotNull(invitationList);
        Assert.assertEquals(1, invitationList.size());

        Invitation invitationFoundInRepo = invitationList.get(0);
        Assert.assertEquals(timestamp, invitationFoundInRepo.getCreatedAt());
    }

}
