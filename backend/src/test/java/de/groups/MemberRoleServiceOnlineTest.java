package de.groups;

import de.groups.models.MemberRole;
import de.groups.models.Permission;
import de.groups.services.MemberRoleService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("online-test")
@ContextConfiguration(classes = {OnlineTestConfig.class}, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class MemberRoleServiceOnlineTest {

    @Autowired
    private MemberRoleService memberRoleService;

    @BeforeEach
    public void setUp() throws Exception {
        Assert.assertNotNull(memberRoleService);

        memberRoleService.deleteAll();
    }

    @AfterEach
    public void tearDown() throws Exception {
        memberRoleService.deleteAll();
    }

    @Test
    public void testSaveMemberRoleSuccess() {
        String name = "Test Mitglieder Rolle";
        Permission[] permissions = Permission.getStandardPermissions();

        MemberRole memberRole = new MemberRole(name, permissions);

        memberRoleService.save(memberRole);

        List<MemberRole> memberRoleList = memberRoleService.findAll();
        Assert.assertNotNull(memberRoleList);
        Assert.assertEquals(1, memberRoleList.size());

        MemberRole memberRoleFoundInRepo = memberRoleList.get(0);
        Assert.assertEquals(name, memberRoleFoundInRepo.getName());
        Assert.assertArrayEquals(permissions, memberRoleFoundInRepo.getPermissions());
    }
}
