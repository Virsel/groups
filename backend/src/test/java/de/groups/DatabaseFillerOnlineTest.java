package de.groups;

import de.groups.helpers.seeder.DatabaseFiller;
import de.groups.services.*;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("online-test")
@ContextConfiguration(classes = {OnlineTestConfig.class}, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class DatabaseFillerOnlineTest {

    @Autowired
    private DatabaseFiller databaseFiller;

    @Autowired
    private MemberRoleService memberRoleService;

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private GroupMemberService groupMemberService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private UserEntryRequestService userEntryRequestService;

    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private GroupEntryRequestService groupEntryRequestService;

    @BeforeEach
    public void setUp() throws Exception {
        Assert.assertNotNull(memberRoleService);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(groupService);
        Assert.assertNotNull(groupMemberService);
        Assert.assertNotNull(invitationService);
        Assert.assertNotNull(userEntryRequestService);
        Assert.assertNotNull(pieceOfNewsService);
        Assert.assertNotNull(groupEntryRequestService);

        groupEntryRequestService.deleteAll();
        userEntryRequestService.deleteAll();
        invitationService.deleteAll();
        pieceOfNewsService.deleteAll();
        groupService.deleteAll();
        groupMemberService.deleteAll();
        userService.deleteAll();
        memberRoleService.deleteAll();
    }

    @AfterEach
    public void tearDown() throws Exception {
        groupEntryRequestService.deleteAll();
        userEntryRequestService.deleteAll();
        invitationService.deleteAll();
        pieceOfNewsService.deleteAll();
        groupService.deleteAll();
        groupMemberService.deleteAll();
        userService.deleteAll();
        memberRoleService.deleteAll();
    }

    @Test
    public void testDatabaseFiller() {
        databaseFiller.fillDatabase();
    }
}
