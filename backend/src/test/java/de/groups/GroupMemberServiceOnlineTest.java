package de.groups;

import de.groups.models.*;
import de.groups.services.GroupMemberService;
import de.groups.services.GroupService;
import de.groups.services.MemberRoleService;
import de.groups.services.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("online-test")
@ContextConfiguration(classes = {OnlineTestConfig.class}, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class GroupMemberServiceOnlineTest {

    @Autowired
    private GroupMemberService groupMemberService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private MemberRoleService memberRoleService;

    @Autowired
    private UserService userService;

    private Timestamp timestamp;

    private MemberRole standardMemberRole;

    private MemberRole moderatorMemberRole;

    private MemberRole adminMemberRole;

    @BeforeEach
    public void setUp() throws Exception {
        Assert.assertNotNull(groupMemberService);
        Assert.assertNotNull(groupService);
        Assert.assertNotNull(memberRoleService);
        Assert.assertNotNull(userService);

        timestamp = new Timestamp(new Date().getTime());

        groupService.deleteAll();
        groupMemberService.deleteAll();
        memberRoleService.deleteAll();
        userService.deleteAll();

        standardMemberRole = memberRoleService.save(new MemberRole("Standard", Permission.getStandardPermissions()));
        moderatorMemberRole = memberRoleService.save(new MemberRole("Moderator", Permission.getModeratorPermissions()));
        adminMemberRole = memberRoleService.save(new MemberRole("Admin", Permission.getAdminPermissions()));
    }

    @AfterEach
    public void tearDown() throws Exception {
        timestamp = null;

        groupService.deleteAll();
        groupMemberService.deleteAll();
        memberRoleService.deleteAll();
        userService.deleteAll();
    }

    @Test
    public void testSaveGroupMemberSuccess() {
        User user = userService.save(new User("Test User"));

        Group group = groupService.save(new Group("Test Gruppe", timestamp, "Test Kategorie", "Test Beschreibung", user));

        GroupMember groupMember = new GroupMember();

        groupMember.setRoles(List.of(standardMemberRole, moderatorMemberRole, adminMemberRole));

        List<GroupMember> groupMembersOfUser = user.getGroupRepresentations();
        groupMembersOfUser.add(groupMember);
        user.setGroupRepresentations(groupMembersOfUser);

        List<GroupMember> groupMembersOfGroup = group.getMembers();
        groupMembersOfGroup.add(groupMember);
        group.setMembers(groupMembersOfGroup);

        groupMemberService.save(groupMember);

        List<GroupMember> groupMemberList = groupMemberService.findAll();
        Assert.assertNotNull(groupMemberList);
        Assert.assertEquals(1, groupMemberList.size());

        GroupMember groupMemberFoundInRepo = groupMemberList.get(0);
        Assert.assertNotNull(groupMemberFoundInRepo.getGroupMemberId());
        Assert.assertEquals(3, groupMemberFoundInRepo.getRoles().size());
    }
}
