package de.groups;

import de.groups.dtos.GroupDTO;
import de.groups.models.Group;
import de.groups.models.User;
import de.groups.services.GroupService;
import de.groups.services.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("online-test")
@ContextConfiguration(classes = {OnlineTestConfig.class}, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class GroupServiceOnlineTest {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    private Timestamp timestamp;

    @BeforeEach
    public void setUp() throws Exception {
        Assert.assertNotNull(groupService);
        Assert.assertNotNull(userService);

        timestamp = new Timestamp(new Date().getTime());

        groupService.deleteAll();
        userService.deleteAll();
    }

    @AfterEach
    public void tearDown() throws Exception {
        timestamp = null;

        groupService.deleteAll();
        userService.deleteAll();
    }

    @Test
    public void testSaveGroupSuccess() {
        User user = userService.save(new User("Test User"));

        String groupName = "Test Gruppe";
        String groupCategory = "Test Kategorie";
        String groupDescription = "Test Beschreibung";

        Group group = new Group(groupName, timestamp, groupCategory, groupDescription, user);

        groupService.save(group);

        List<GroupDTO> groupList = groupService.findAll();
        Assert.assertNotNull(groupList);
        Assert.assertEquals(1, groupList.size());

        GroupDTO groupFoundInRepo = groupList.get(0);
        Assert.assertEquals(groupName, groupFoundInRepo.getName());
        Assert.assertEquals(groupCategory, groupFoundInRepo.getCategory());
        Assert.assertEquals(groupDescription, groupFoundInRepo.getDescription());
    }
}
