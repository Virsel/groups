package de.groups;

import de.groups.models.User;
import de.groups.services.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("online-test")
@ContextConfiguration(classes = {OnlineTestConfig.class}, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class UserServiceOnlineTest {

    @Autowired
    private UserService userService;

    @BeforeEach
    public void setUp() throws Exception {
        Assert.assertNotNull(userService);

        userService.deleteAll();
    }

    @AfterEach
    public void tearDown() throws Exception {
        userService.deleteAll();
    }

    @Test
    public void testSaveUserSuccess() {
        String userName = "Test User";

        User user = new User(userName);

        userService.save(user);

        List<User> userList = userService.findAll();
        Assert.assertNotNull(userList);
        Assert.assertEquals(1, userList.size());

        User userFoundInRepo = userList.get(0);
        Assert.assertEquals(userName, userFoundInRepo.getName());
    }
}
