package de.groups;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:test-application.properties")
@ComponentScan(basePackages = {"de.groups"})
@Profile("online-test")
public class OnlineTestConfig {
}
