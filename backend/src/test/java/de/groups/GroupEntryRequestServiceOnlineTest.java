package de.groups;

import de.groups.models.Group;
import de.groups.models.GroupEntryRequest;
import de.groups.models.GroupEntryRequestEnum;
import de.groups.models.User;
import de.groups.services.GroupEntryRequestService;
import de.groups.services.GroupService;
import de.groups.services.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("online-test")
@ContextConfiguration(classes = {OnlineTestConfig.class}, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class GroupEntryRequestServiceOnlineTest {

    @Autowired
    private GroupEntryRequestService groupEntryRequestService;

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    private Timestamp timestamp;

    @BeforeEach
    public void setUp() throws Exception {
        Assert.assertNotNull(groupEntryRequestService);
        Assert.assertNotNull(userService);
        Assert.assertNotNull(groupService);

        timestamp = new Timestamp(new Date().getTime());

        groupEntryRequestService.deleteAll();
        groupService.deleteAll();
        userService.deleteAll();
    }

    @AfterEach
    public void tearDown() throws Exception {
        timestamp = null;

        groupEntryRequestService.deleteAll();
        groupService.deleteAll();
        userService.deleteAll();
    }


    @Test
    public void testSaveGroupEntryRequestSuccess() {
        User user = userService.save(new User("Test User"));

        Group group = groupService.save(new Group("Test Gruppe", timestamp, "Test Kategorie", "Test Beschreibung", user));

        GroupEntryRequest groupEntryRequest = new GroupEntryRequest();

        groupEntryRequest.setCreatedAt(timestamp);
        groupEntryRequest.setRelatedGroup(group);

        groupEntryRequestService.save(groupEntryRequest);

        List<GroupEntryRequest> groupEntryRequestList = groupEntryRequestService.findAll();
        Assert.assertNotNull(groupEntryRequestList);
        Assert.assertEquals(1, groupEntryRequestList.size());

        GroupEntryRequest groupEntryRequestFoundInRepo = groupEntryRequestList.get(0);
        Assert.assertEquals(timestamp, groupEntryRequestFoundInRepo.getCreatedAt());
        Assert.assertEquals(groupEntryRequestFoundInRepo.getStatus(), GroupEntryRequestEnum.IN_PROGRESS);
    }

}
