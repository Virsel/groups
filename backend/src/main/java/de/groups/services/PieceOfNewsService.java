package de.groups.services;

import de.groups.dtos.PieceOfNewsDTO;
import de.groups.models.PieceOfNews;
import de.groups.repositories.PieceOfNewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PieceOfNewsService {

    @Autowired
    private PieceOfNewsRepository pieceOfNewsRepository;

    @Transactional
    public List<PieceOfNews> findAll() {
        return pieceOfNewsRepository.findAll();
    }

    @Transactional
    public Optional<PieceOfNews> findById(String pieceOfNewsID) {
        return pieceOfNewsRepository.findById(pieceOfNewsID);
    }

    @Transactional
    public List<PieceOfNewsDTO> findByGroup(String groupId) {
        return pieceOfNewsRepository.getFlatListByRelatedGroupId(groupId);
    }

    @Transactional
    public List<PieceOfNewsDTO> getAllAsFlatList() {
        return pieceOfNewsRepository.getAllAsFlatList();
    }

    @Transactional
    public Integer getCountByUserName(String userName) {
        return pieceOfNewsRepository.getCountByUserName(userName);
    }

    @Transactional
    public PieceOfNews save(PieceOfNews pieceOfNews) {
        return pieceOfNewsRepository.save(pieceOfNews);
    }

    @Transactional
    public void delete(PieceOfNews pieceOfNews) {
        pieceOfNewsRepository.delete(pieceOfNews);
    }

    @Transactional
    public void deleteAll() {
        pieceOfNewsRepository.deleteAll();
    }
}
