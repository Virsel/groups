package de.groups.services;

import de.groups.models.MemberRole;
import de.groups.repositories.MemberRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class MemberRoleService {

    @Autowired
    private MemberRoleRepository memberRoleRepository;

    @Transactional
    public List<MemberRole> findAll() {
        return memberRoleRepository.findAll();
    }

    @Transactional
    public Optional<MemberRole> findByName(String name) {
        return memberRoleRepository.findByName(name);
    }

    @Transactional
    public Optional<MemberRole> findById(String id) {
        return memberRoleRepository.findById(id);
    }

    @Transactional
    public MemberRole save(MemberRole memberRole) {
        return memberRoleRepository.save(memberRole);
    }

    @Transactional
    public void deleteAll() {
        memberRoleRepository.deleteAll();
    }

}
