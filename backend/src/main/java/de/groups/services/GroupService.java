package de.groups.services;

import de.groups.dtos.GroupDTO;
import de.groups.dtos.GroupDetailDTO;
import de.groups.dtos.GroupOfMemberDTO;
import de.groups.models.Group;
import de.groups.repositories.GroupMemberRepository;
import de.groups.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Transactional
    public List<GroupDTO> findAll() {
        return groupRepository.findAllAsDTO();
    }

    @Transactional
    public Optional<Group> findById(String groupId) {
        return groupRepository.findById(groupId);
    }


    @Transactional
    public GroupOfMemberDTO findGroupForGroupMember(String groupMemberId) {
        var group = groupRepository.findByGroupMemberId(groupMemberId);
        var roles = groupMemberRepository.getRoles(groupMemberId);
        group.setRoles(roles);
        return group;
    }

    @Transactional
    public Optional<GroupDTO> findByName(String name) {
        return groupRepository.findByName(name);
    }

    @Transactional
    public Optional<GroupDetailDTO> findByName2(String name) {
        return groupRepository.findByName2(name);
    }


    @Transactional
    public Group save(Group group) {
        return groupRepository.save(group);
    }

    @Transactional
    public void delete(Group group) {
        groupRepository.delete(group);
    }

    @Transactional
    public void deleteAll() {
        groupRepository.deleteAll();
    }

}
