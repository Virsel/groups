package de.groups.services;

import de.groups.dtos.GroupEntryRequestDTO;
import de.groups.models.User;
import de.groups.models.UserEntryRequest;
import de.groups.repositories.UserEntryRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserEntryRequestService {

    @Autowired
    private UserEntryRequestRepository userEntryRequestRepository;

    @Transactional
    public List<UserEntryRequest> findAll() {
        return userEntryRequestRepository.findAll();
    }

    @Transactional
    public UserEntryRequest save(UserEntryRequest userEntryRequest) {
        return userEntryRequestRepository.save(userEntryRequest);
    }

    @Transactional
    public List<GroupEntryRequestDTO> getAllByIssuer(User issuer) {
        return userEntryRequestRepository.findByIssuer(issuer);
    }

    @Transactional
    public void deleteAll() {
        userEntryRequestRepository.deleteAll();
    }

}
