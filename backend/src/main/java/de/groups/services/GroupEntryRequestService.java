package de.groups.services;

import de.groups.models.GroupEntryRequest;
import de.groups.repositories.GroupEntryRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class GroupEntryRequestService {

    @Autowired
    private GroupEntryRequestRepository groupEntryRequestRepository;

    @Transactional
    public List<GroupEntryRequest> findAll() {
        return groupEntryRequestRepository.findAll();
    }

    @Transactional
    public GroupEntryRequest save(GroupEntryRequest groupEntryRequest) {
        return groupEntryRequestRepository.save(groupEntryRequest);
    }

    @Transactional
    public void deleteAll() {
        groupEntryRequestRepository.deleteAll();
    }

}
