package de.groups.services;

import de.groups.models.Invitation;
import de.groups.models.User;
import de.groups.repositories.InvitationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class InvitationService {

    @Autowired
    private InvitationRepository invitationRepository;

    @Transactional
    public List<Invitation> findAll() {
        return invitationRepository.findAll();
    }

    @Transactional
    public List<Invitation> findAllByRelatedUser(User relatedUser) {
        return invitationRepository.findByRelatedUser(relatedUser);
    }

    @Transactional
    public Invitation save(Invitation invitation) {
        return invitationRepository.save(invitation);
    }

    @Transactional
    public void deleteAll() {
        invitationRepository.deleteAll();
    }

}
