package de.groups.services;

import de.groups.dtos.ForeignUserDTO;
import de.groups.models.User;
import de.groups.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional
    public boolean existsByName(String name) {
       return userRepository.existsUserByName(name);
    }

    @Transactional
    public Optional<String> getIdByName(String name) {
        return userRepository.getIdByName(name);
    }

    @Transactional
    public List<ForeignUserDTO> getUsersNotInGroup(String groupName) {
        return userRepository.getUsersNotInGroup(groupName);
    }

    @Transactional
    public Optional<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Transactional
    public void deleteAll() {
        userRepository.deleteAll();
    }

}
