package de.groups.services;

import de.groups.dtos.GroupMemberDTO;
import de.groups.models.GroupMember;
import de.groups.models.MemberRole;
import de.groups.repositories.GroupMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class GroupMemberService {

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Transactional
    public List<GroupMember> findAll() {
        return groupMemberRepository.findAll();
    }

    @Transactional
    public Optional<GroupMember> findById(String id) {
        return groupMemberRepository.findById(id);
    }

    @Transactional
    public List<GroupMemberDTO> findByGroupId(String groupId) {
        var res = groupMemberRepository.findByGroupId(groupId);
        for (GroupMemberDTO memberDTO : res) {
            var roles = getRoles(memberDTO.getGroupMemberId());
            memberDTO.setRoles(roles);
        }

        return res;
    }

    public String getGroupId(String memberId){
        return groupMemberRepository.getGroupId(memberId);
    }

    public List<MemberRole> getRoles(String groupMemberId) {
        return groupMemberRepository.getRoles(groupMemberId);
    }

    public void setMemberRolesById(List<MemberRole> memberRoleList, String groupMemberId) {
        groupMemberRepository.setRolesById(memberRoleList, groupMemberId);
    }

    @Transactional
    public GroupMember save(GroupMember groupMember) {
        return groupMemberRepository.save(groupMember);
    }

    @Transactional
    public void deleteAll() {
        groupMemberRepository.deleteAll();
    }

    @Transactional
    public String getGroupMemberIdByGroupIdAndMemberName(String groupId, String memberName) {
        return groupMemberRepository.getGroupMemberIdByGroupIdAndMemberName(groupId, memberName);
    }

    @Transactional
    public List<String> getGroupMemberIdsByUserName(String username) {
        return groupMemberRepository.getGroupMemberIdsByUserName(username);
    }


    @Transactional
    public void setDeletedById(boolean deleted, String id) {
        groupMemberRepository.setGroupMemberDeletedById(deleted, id);
    }
}
