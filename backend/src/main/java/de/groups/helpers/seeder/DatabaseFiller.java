package de.groups.helpers.seeder;

import com.github.javafaker.Faker;
import de.groups.MitgliederVerwaltungMain;
import de.groups.models.*;
import de.groups.services.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class DatabaseFiller {

    @Autowired
    private GroupEntryRequestService groupEntryRequestService;

    @Autowired
    private GroupMemberService groupMemberService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private MemberRoleService memberRoleService;


    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private UserEntryRequestService userEntryRequestService;

    @Autowired
    private UserService userService;

    private SessionFactory hibernateFactory;


    private final Faker faker;

    private final Random random;

    private final Timestamp timestamp;

    private MemberRole standardMemberRole;

    private MemberRole moderatorMemberRole;

    private MemberRole adminMemberRole;

    private User userPaul;


    private final List<String> possibleCategories = List.of("Wissenschaft",
            "Business",
            "Finanzen",
            "IT",
            "Medizin",
            "General",
            "Reise",
            "Mobilität",
            "Politik",
            "Sicherheit",
            "Natur",
            "Essen",
            "Tourismus",
            "Sport");

    private Transaction tx;
    private Session session;

    private List<User> savedUsers = new ArrayList<>();
    private List<User> savedDynamicUsers = new ArrayList<>();
    private User staticUserPaul;
    private Group staticGroupFromPaul;
    private User staticUserMarvin;
    private Group staticGroupFromMarvin;
    private List<Group> savedGroups = new ArrayList<>();

    private List<Invitation> savedInvitations = new ArrayList<>();

    public DatabaseFiller() {
        faker = new Faker(Locale.GERMAN);
        random = new Random();
        timestamp = new Timestamp(new Date().getTime());
    }

    @Autowired
    public DatabaseFiller(EntityManagerFactory factory) {
        this();
        if (factory.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        this.hibernateFactory = factory.unwrap(SessionFactory.class);

    }

    @Transactional
    public void fillDatabase() {
//        Session session = openSession();
//        SessionFactory sessionFactory = new SessionFactoryBuilderService();
        deleteDatabaseRecords();
        session = this.hibernateFactory.openSession();
        this.tx = session.getTransaction();
        this.tx.begin();
        createMemberRoles();
        this.createUsers();
        this.createGroups();
        this.createInvitations();
        this.createUserEntryRequests();
        tx.commit();
        session.close();
    }

    public void createInvitations() {
        createInvitationsForStaticUsers();
        for (int i = 0; i < savedGroups.size(); i++) {
            var group = savedGroups.get(i);
            // get users which aren't in group
            var possibleUsersToInvite = savedDynamicUsers.stream().
                    filter(x -> !group.getMembers().stream().anyMatch(y -> y.getUser().getName().equals(x.getName())))
                    .collect(Collectors.toList());
            Collections.shuffle(possibleUsersToInvite);
            var count = random.nextInt(3 - 1) + 1;
            for (int j = 0; j < count; j++) {
                var invitation = new Invitation();
                invitation.setCreatedAt(timestamp);
                invitation.setRelatedGroup(group);
                invitation.setStatus(GroupEntryRequestEnum.IN_PROGRESS);
                List<GroupMember> forInvitationEligibleMember = group.getMembers().stream().filter(x -> x.isAdmin() || x.isModerator()).collect(Collectors.toList());
                invitation.setIssuer(forInvitationEligibleMember.get(random.nextInt(forInvitationEligibleMember.size())));
                invitation.setRelatedUser(possibleUsersToInvite.get(j));
                this.savedInvitations.add(invitation);
                this.session.persist(invitation);
            }
        }
    }

    public void createInvitationsForStaticUsers() {
        // invitations for user paul
        var possibleInviterGroups = savedGroups.stream().
                filter(x -> !x.getMembers().stream().anyMatch(y -> y.getUser() == staticUserPaul))
                .collect(Collectors.toList());
        Collections.shuffle(possibleInviterGroups);
        var count = random.nextInt(6 - 3) + 3;
        for (int i = 0; i < count; i++) {
            var inviterGroup = possibleInviterGroups.get(i);
            var invitation = new Invitation();
            invitation.setCreatedAt(timestamp);
            invitation.setRelatedGroup(inviterGroup);
            invitation.setStatus(GroupEntryRequestEnum.IN_PROGRESS);
            List<GroupMember> forInvitationEligibleMember = inviterGroup.getMembers().stream().filter(x -> x.isAdmin() || x.isModerator()).collect(Collectors.toList());
            invitation.setIssuer(forInvitationEligibleMember.get(random.nextInt(forInvitationEligibleMember.size())));
            invitation.setRelatedUser(staticUserPaul);
            this.savedInvitations.add(invitation);
            this.session.persist(invitation);
        }

        // invitations for user marvin
        possibleInviterGroups = savedGroups.stream().
                filter(x -> !x.getMembers().stream().anyMatch(y -> y.getUser() == staticUserMarvin))
                .collect(Collectors.toList());
        Collections.shuffle(possibleInviterGroups);
        count = random.nextInt(3 - 1) + 1;
        for (int i = 0; i < count; i++) {
            var inviterGroup = possibleInviterGroups.get(i);
            var invitation = new Invitation();
            invitation.setCreatedAt(timestamp);
            invitation.setRelatedGroup(inviterGroup);
            invitation.setStatus(GroupEntryRequestEnum.IN_PROGRESS);
            List<GroupMember> forInvitationEligibleMember = inviterGroup.getMembers().stream().filter(x -> x.isAdmin() || x.isModerator()).collect(Collectors.toList());
            invitation.setIssuer(forInvitationEligibleMember.get(random.nextInt(forInvitationEligibleMember.size())));
            invitation.setRelatedUser(staticUserMarvin);
            this.savedInvitations.add(invitation);
            this.session.persist(invitation);
        }
    }

    public void createUserEntryRequestsForStaticUsers() {
        // entry requests for user paul
        var notPossibleGroups = savedInvitations.stream().
                filter(x -> x.getRelatedUser() == staticUserPaul)
                .map(x -> x.getRelatedGroup())
                .collect(Collectors.toList());
        var possibleGroupsForEntryRequest = savedGroups.stream().
                filter(x -> !x.getMembers().stream().anyMatch(y -> y.getUser() == staticUserPaul) && !notPossibleGroups.contains(x))
                .collect(Collectors.toList());
        Collections.shuffle(possibleGroupsForEntryRequest);
        var count = random.nextInt(6 - 3) + 3;
        for (int i = 0; i < count; i++) {
            var groupForEntryRequest = possibleGroupsForEntryRequest.get(i);
            var entryRequest = new UserEntryRequest();
            entryRequest.setStatus(GroupEntryRequestEnum.IN_PROGRESS);
            entryRequest.setCreatedAt(timestamp);
            entryRequest.setIssuer(staticUserPaul);
            entryRequest.setRelatedGroup(groupForEntryRequest);
            this.session.persist(entryRequest);
        }

        // entry requests for user marvin
        var forMarvinNotPossibleGroups = savedInvitations.stream().
                filter(x -> x.getRelatedUser() == staticUserMarvin)
                .map(x -> x.getRelatedGroup())
                .collect(Collectors.toList());
        possibleGroupsForEntryRequest = savedGroups.stream().
                filter(x -> !x.getMembers().stream().anyMatch(y -> y.getUser() == staticUserPaul) && !forMarvinNotPossibleGroups.contains(x))
                .collect(Collectors.toList());
        Collections.shuffle(possibleGroupsForEntryRequest);
        count = random.nextInt(6 - 3) + 3;
        for (int i = 0; i < count; i++) {
            var groupForEntryRequest = possibleGroupsForEntryRequest.get(i);
            var entryRequest = new UserEntryRequest();
            entryRequest.setStatus(GroupEntryRequestEnum.IN_PROGRESS);
            entryRequest.setCreatedAt(timestamp);
            entryRequest.setIssuer(staticUserMarvin);
            entryRequest.setRelatedGroup(groupForEntryRequest);
            this.session.persist(entryRequest);
        }
    }

    public void createUserEntryRequests() {
        createUserEntryRequestsForStaticUsers();
        for (int i = 0; i < savedDynamicUsers.size(); i++) {
            var user = savedDynamicUsers.get(i);
            var notPossibleGroups = savedInvitations.stream().
                    filter(x -> x.getRelatedUser() == user)
                    .map(x -> x.getRelatedGroup())
                    .collect(Collectors.toList());
            var possibleGroupsForEntryRequest = savedGroups.stream().
                    filter(x -> !x.getMembers().stream().anyMatch(y -> y.getUser() == user) && !notPossibleGroups.contains(x))
                    .collect(Collectors.toList());
            Collections.shuffle(possibleGroupsForEntryRequest);
            var count = random.nextInt(6 - 3) + 3;
            for (int j = 0; j < count; j++) {
                var groupForEntryRequest = possibleGroupsForEntryRequest.get(j);
                var entryRequest = new UserEntryRequest();
                entryRequest.setStatus(GroupEntryRequestEnum.IN_PROGRESS);
                entryRequest.setCreatedAt(timestamp);
                entryRequest.setIssuer(user);
                entryRequest.setRelatedGroup(groupForEntryRequest);
                this.session.persist(entryRequest);
            }
        }
    }

    public void createStaticUsers() {
        this.staticUserPaul = new User();
        this.staticUserPaul.setName("Paul");
        this.session.persist(this.staticUserPaul);
        this.savedUsers.add(this.staticUserPaul);

        this.staticUserMarvin = new User();
        this.staticUserMarvin.setName("Marvin");
        this.session.persist(this.staticUserMarvin);
        this.savedUsers.add(this.staticUserMarvin);
    }

    public void createUsers() {
        createStaticUsers();
        for (int i = 0; i < 150; i++) {
            User user = new User();
            String username = null;
            var userAlreadyExists = true;
            while (userAlreadyExists) {
                String testName = faker.name().firstName();
                userAlreadyExists = this.savedUsers.stream().anyMatch(x -> testName.equals(x.getName()));
                username = testName;
            }
            user.setName(username);
            this.session.persist(user);
            this.savedDynamicUsers.add(user);
            this.savedUsers.add(user);
        }
    }

    public void createStaticGroups() {
        // group from paul
        staticGroupFromPaul = new Group();
        staticGroupFromPaul.setName("Roboter4You");
        staticGroupFromPaul.setCategory("Robotik");
        staticGroupFromPaul.setDescription("Hier geht es darum, Neuigkeiten rund um das Thema künstliche Intelligenz im " +
                "Zusammenhang mit Robotern mit Gleichgesinnten zu teilen. Im Fordergrund liegt dabei die Entwicklung " +
                "von Robotern, welche dem Menschen im Alltag behilflich sein können.");
        staticGroupFromPaul.setCreatedAt(timestamp);

        var members = getAndCreateGroupMembersForPaulGroup(staticGroupFromPaul);
        staticGroupFromPaul.setMembers(members);

        staticGroupFromPaul.setFounder(staticUserPaul);

        this.session.persist(staticUserPaul);
        this.session.persist(staticGroupFromPaul);
        savedGroups.add(staticGroupFromPaul);

        // group from marvin
        staticGroupFromMarvin = new Group();
        staticGroupFromMarvin.setName("MaxNews");
        staticGroupFromMarvin.setCategory("Robotik");
        staticGroupFromMarvin.setDescription(faker.lorem().paragraph(4));
        staticGroupFromMarvin.setCreatedAt(timestamp);

        members = getAndCreateGroupMembersForMarvinGroup(staticGroupFromMarvin);
        staticGroupFromMarvin.setMembers(members);

        staticGroupFromMarvin.setFounder(staticUserMarvin);

        this.session.persist(staticUserMarvin);
        this.session.persist(staticGroupFromMarvin);
        savedGroups.add(staticGroupFromMarvin);
    }

    public List<GroupMember> getAndCreateGroupMembersForPaulGroup(Group g) {
        var count = random.nextInt(10 - 6) + 6;
        Collections.shuffle(this.savedUsers);
        var res = new ArrayList<GroupMember>();
        for (int i = 0; i < count; i++) {
            User user = this.savedUsers.get(i);
            if(user == staticUserPaul)
            {
                continue;
            }
            var groupMember = new GroupMember();
            groupMember.setGroup(g);
            groupMember.addRole(standardMemberRole);
            groupMember.setUser(user);
            // add to two of them moderator role
            if (i == 0 || i == 1) {
                groupMember.addRole(moderatorMemberRole);
            }
            this.session.persist(groupMember);
            user.addGroupRepresentation(groupMember);
            this.createListOfNewsForGroupMember(groupMember);
            res.add(groupMember);
        }

        var groupMember = new GroupMember();
        groupMember.setGroup(g);
        groupMember.addRole(standardMemberRole);
        groupMember.setUser(staticUserPaul);
        groupMember.addRole(adminMemberRole);
        this.session.persist(groupMember);
        staticUserPaul.addGroupRepresentation(groupMember);
        this.createListOfNewsForGroupMember(groupMember);
        res.add(groupMember);
        return res;
    }

    public List<GroupMember> getAndCreateGroupMembersForMarvinGroup(Group g) {
        var count = random.nextInt(10 - 6) + 6;
        Collections.shuffle(this.savedUsers);
        var res = new ArrayList<GroupMember>();
        for (int i = 0; i < count; i++) {
            User user = this.savedUsers.get(i);
            if(user == staticUserMarvin)
            {
                continue;
            }
            var groupMember = new GroupMember();
            groupMember.setGroup(g);
            groupMember.addRole(standardMemberRole);
            groupMember.setUser(user);
            // add to two of them moderator role
            if (i == 0 || i == 1) {
                groupMember.addRole(moderatorMemberRole);
            }
            this.session.persist(groupMember);
            user.addGroupRepresentation(groupMember);
            this.createListOfNewsForGroupMember(groupMember);
            res.add(groupMember);
        }

        var groupMember = new GroupMember();
        groupMember.setGroup(g);
        groupMember.addRole(standardMemberRole);
        groupMember.setUser(staticUserMarvin);
        groupMember.addRole(adminMemberRole);
        this.session.persist(groupMember);
        staticUserMarvin.addGroupRepresentation(groupMember);
        this.createListOfNewsForGroupMember(groupMember);
        res.add(groupMember);
        return res;
    }

    public void createGroups() {
        createStaticGroups();
        for (int i = 0; i < 20; i++) {
            Group group = new Group();
            var groupAlreadyExists = true;
            String groupName = null;
            while (groupAlreadyExists) {
                String testName = faker.funnyName().name();
                groupAlreadyExists = this.savedGroups.stream().anyMatch(x -> testName.equals(x.getName()));
                groupName = testName;
            }
            group.setName(groupName);
            group.setCategory(possibleCategories.get(random.nextInt(possibleCategories.size())));
            group.setCreatedAt(timestamp);
            group.setDescription(faker.lorem().paragraph(random.nextInt(4 - 2) + 2));
            var members = getAndCreateGroupMembers(group);

            group.setMembers(members);
            // make one of them to founder/admin
            var founder = members.get(random.nextInt(members.size()));
            founder.addRole(adminMemberRole);
            group.setFounder(founder.getUser());

            this.session.persist(founder);
            this.session.persist(group);
            savedGroups.add(group);
        }
    }

    public List<GroupMember> getAndCreateGroupMembers(Group g) {
        var count = random.nextInt(10 - 6) + 6;
        Collections.shuffle(this.savedUsers);
        var res = new ArrayList<GroupMember>();
        for (int i = 0; i < count; i++) {
            var groupMember = new GroupMember();
            groupMember.setGroup(g);
            groupMember.addRole(standardMemberRole);
            groupMember.setUser(this.savedUsers.get(i));
            // add to two of them moderator role
            if (i == 0 || i == 1) {
                groupMember.addRole(moderatorMemberRole);
            }
            this.session.persist(groupMember);
            this.savedUsers.get(i).addGroupRepresentation(groupMember);
            this.createListOfNewsForGroupMember(groupMember);
            res.add(groupMember);
        }
        return res;
    }

    public void createListOfNewsForGroupMember(GroupMember groupMember) {
        var count = random.nextInt(10 - 3) + 3;
        for (int i = 0; i < count; i++) {
            var pieceOfNews = new PieceOfNews();
            pieceOfNews.setCreatedAt(timestamp);
            pieceOfNews.setCreator(groupMember);
            pieceOfNews.setRelatedGroup(groupMember.getGroup());
            pieceOfNews.setContent(faker.lorem().paragraph(random.nextInt(5 - 3) + 3));
            pieceOfNews.setTitle(faker.book().title());
            this.session.persist(pieceOfNews);
        }
    }


//    public List<User> fillDbWithUsers(){
//        for (int i = 0; i < 150; i++) {
//            User user = new User();
//            user.setName(faker.name().fullName());
//            usersToInvite.add(userService.save(user));
//        }
//    }

    public void fillDatabaseWithMockupData() {
        List<User> usersToInvite = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setName(faker.name().fullName());
            this.session.persist(user);
            usersToInvite.add(user);
        }

        List<User> userToIssueUserEntryRequests = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setName(faker.name().fullName());
            this.session.persist(user);
            userToIssueUserEntryRequests.add(user);
        }

        List<User> userToAddToGroupRoboterForYou = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setName(faker.name().fullName());
            this.session.persist(user);
            userToAddToGroupRoboterForYou.add(user);
        }

        User tmpUserPaul = new User("Paul");
        this.session.persist(tmpUserPaul);

        String groupNameRoboterForYou = "Roboter4You";
        String groupCategoryRoboterForYou = "Robotik";
        String groupDescriptionRoboterForYou = "Hier geht es darum, Neuigkeiten rund um das Thema künstliche Intelligenz im Zusammenhang mit Robotern mit Gleichgesinnten zu teilen. Im Fordergrund liegt dabei die Entwicklung von Robotern, welche dem Menschen im Alltag behilflich sein können.";
        Group groupRoboterForYou = new Group(groupNameRoboterForYou, this.timestamp, groupCategoryRoboterForYou, groupDescriptionRoboterForYou);

        this.session.persist(groupRoboterForYou);
        GroupMember groupMemberPaulInRoboterForYou = addAdminUserToGroup(tmpUserPaul, groupRoboterForYou);

        this.userPaul = tmpUserPaul;

        groupRoboterForYou.setFounder(this.userPaul);

        for (User u : userToAddToGroupRoboterForYou) {
            addStandardUserToGroup(u, groupRoboterForYou);
            this.session.persist(u);
        }

        this.session.persist(groupRoboterForYou);

        List<GroupMember> groupMembers = groupRoboterForYou.getMembers();
        for (GroupMember gm : groupMembers) {
            int min = 2, max = 4;
            int randInt = random.nextInt((max - min) + 1) + min;

            for (int i = 0; i < randInt; i++) {
                createPieceOfNews(gm, groupRoboterForYou);
            }
        }

        for (User u : usersToInvite) {
            createInvitations(u, groupRoboterForYou, groupMemberPaulInRoboterForYou);
        }

        for (User u : userToIssueUserEntryRequests) {
            createUserEntryRequest(u, groupRoboterForYou);
        }
    }

    public void fillDatabaseWithRandomUserData() {

        List<User> randomUser = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            User user = new User(faker.name().fullName());
            this.session.persist(user);
            randomUser.add(user);
        }

        List<Group> randomGroups = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = randomUser.get(i);

            String groupName = "Group " + i + ": " + faker.funnyName().name();
            String groupCategory = possibleCategories.get(random.nextInt(possibleCategories.size()));
            String groupDescription = "Automatisch erstellte Gruppe für " + user.getName() + " zum Thema " + groupCategory;

            Group group = new Group(groupName, timestamp, groupCategory, groupDescription);

            addAdminUserToGroup(user, group);

            this.session.persist(user);

            group.setFounder(user);

            randomGroups.add(group);
        }

        for (int i = 0; i < 10; i++) {
            User user = randomUser.get((5 + i));

            List<Group> groupsToJoin = new ArrayList<>();

            int min = 1, max = 3;
            int randInt = random.nextInt((max - min) + 1) + min;

            Group group = null;
            Group previousGroup = null;
            for (int j = 0; j < randInt; j++) {
                while (group == previousGroup) {
                    int index = random.nextInt(randomGroups.size());
                    group = randomGroups.get(index);
                }

                groupsToJoin.add(group);

                previousGroup = group;
            }

            for (Group groupToJoin : groupsToJoin) {
                addModeratorUserToGroup(user, groupToJoin);
            }

            this.session.persist(user);

        }

        for (int i = 0; i < 15; i++) {
            User user = randomUser.get((15 + i));

            List<Group> groupsToJoin = new ArrayList<>();

            int min = 1, max = 3;
            int randInt = random.nextInt((max - min) + 1) + min;

            Group group = null;
            Group previousGroup = null;
            for (int j = 0; j < randInt; j++) {
                while (group == previousGroup) {
                    int index = random.nextInt(randomGroups.size());
                    group = randomGroups.get(index);
                }

                groupsToJoin.add(group);

                previousGroup = group;
            }

            for (Group groupToJoin : groupsToJoin) {
                addStandardUserToGroup(user, groupToJoin);
            }

            this.session.persist(user);

        }

        List<Group> groupsSaved = new ArrayList<>();
        for (Group group : randomGroups) {
            this.session.persist(group);
            groupsSaved.add(group);
        }

        for (Group group : groupsSaved) {
            for (GroupMember groupMember : group.getMembers()) {
                int min = 1, max = 2;
                int randInt = random.nextInt((max - min) + 1) + min;

                for (int j = 0; j < randInt; j++) {
                    createPieceOfNews(groupMember, group);
                }
            }
        }

        for (Group group : groupsSaved) {
            GroupMember issuer = group.getMembers().get(0);

            createInvitations(userPaul, group, issuer);
        }
    }

    public void deleteDatabaseRecords() {
        groupEntryRequestService.deleteAll();
        userEntryRequestService.deleteAll();
        invitationService.deleteAll();
        pieceOfNewsService.deleteAll();
        memberRoleService.deleteAll();
        groupMemberService.deleteAll();
        groupService.deleteAll();
        userService.deleteAll();

        MitgliederVerwaltungMain.restart();
    }

    private void createMemberRoles() {
        if (memberRoleService.findByName("Standard").isEmpty()) {
            standardMemberRole = new MemberRole("Standard", Permission.getStandardPermissions());
            this.session.persist(standardMemberRole);
        }

        if (memberRoleService.findByName("Moderator").isEmpty()) {
            moderatorMemberRole = new MemberRole("Moderator", Permission.getModeratorPermissions());
            this.session.persist(moderatorMemberRole);
        }

        if (memberRoleService.findByName("Admin").isEmpty()) {
            adminMemberRole = new MemberRole("Admin", Permission.getAdminPermissions());
            this.session.persist(adminMemberRole);
        }
    }

    private void createInvitations(User user, Group group, GroupMember issuer) {
        Invitation invitation = new Invitation(issuer, user);

        invitation.setRelatedGroup(group);
        invitation.setCreatedAt(timestamp);
        invitation.setStatus(GroupEntryRequestEnum.IN_PROGRESS);

        this.session.persist(invitation);
    }

    private void createUserEntryRequest(User user, Group group) {
        UserEntryRequest userEntryRequest = new UserEntryRequest();

        userEntryRequest.setIssuer(user);
        userEntryRequest.setRelatedGroup(group);
        userEntryRequest.setCreatedAt(timestamp);
        userEntryRequest.setStatus(GroupEntryRequestEnum.IN_PROGRESS);

        this.session.persist(userEntryRequest);
    }

    private GroupMember addAdminUserToGroup(User user, Group group) {
        GroupMember tmpGroupMember = new GroupMember();

        List<MemberRole> memberRoles = tmpGroupMember.getRoles();

        memberRoles.add(standardMemberRole);
        memberRoles.add(adminMemberRole);

        tmpGroupMember.setRoles(memberRoles);
        tmpGroupMember.setUser(user);
        tmpGroupMember.setGroup(group);

        this.session.persist(tmpGroupMember);

        List<GroupMember> groupMembersOfUser = user.getGroupRepresentations();
        groupMembersOfUser.add(tmpGroupMember);
        user.setGroupRepresentations(groupMembersOfUser);

        List<GroupMember> groupMembersOfGroup = group.getMembers();
        groupMembersOfGroup.add(tmpGroupMember);
        group.setMembers(groupMembersOfGroup);

        return tmpGroupMember;
    }

    private GroupMember addStandardUserToGroup(User user, Group group) {
        GroupMember tmpGroupMember = new GroupMember();

        List<MemberRole> memberRoles = tmpGroupMember.getRoles();

        memberRoles.add(standardMemberRole);

        tmpGroupMember.setRoles(memberRoles);
        tmpGroupMember.setUser(user);
        tmpGroupMember.setGroup(group);
        this.session.persist(tmpGroupMember);

        List<GroupMember> groupMembersOfUser = user.getGroupRepresentations();
        groupMembersOfUser.add(tmpGroupMember);
        user.setGroupRepresentations(groupMembersOfUser);

        List<GroupMember> groupMembersOfGroup = group.getMembers();
        groupMembersOfGroup.add(tmpGroupMember);
        group.setMembers(groupMembersOfGroup);

        return tmpGroupMember;
    }

    private GroupMember addModeratorUserToGroup(User user, Group group) {
        GroupMember tmpGroupMember = new GroupMember();

        List<MemberRole> memberRoles = tmpGroupMember.getRoles();

        memberRoles.add(standardMemberRole);
        memberRoles.add(moderatorMemberRole);

        tmpGroupMember.setRoles(memberRoles);
        tmpGroupMember.setUser(user);
        tmpGroupMember.setGroup(group);
        this.session.persist(tmpGroupMember);

        List<GroupMember> groupMembersOfUser = user.getGroupRepresentations();
        groupMembersOfUser.add(tmpGroupMember);
        user.setGroupRepresentations(groupMembersOfUser);

        List<GroupMember> groupMembersOfGroup = group.getMembers();
        groupMembersOfGroup.add(tmpGroupMember);
        group.setMembers(groupMembersOfGroup);

        return tmpGroupMember;
    }

    private PieceOfNews createPieceOfNews(GroupMember groupMember, Group group) {
        String title = "Test Nachricht";
        String content = "Für die Gruppe " + group.getName() + " erstellt.";

        PieceOfNews news = new PieceOfNews(title, content, timestamp, group, groupMember);
        this.session.persist(news);
        return news;
    }
}
