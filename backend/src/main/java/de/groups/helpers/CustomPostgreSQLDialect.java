package de.groups.helpers;

import org.hibernate.dialect.PostgreSQL94Dialect;

public class CustomPostgreSQLDialect extends PostgreSQL94Dialect {

    public CustomPostgreSQLDialect() {
        super();
        this.registerColumnType(2003, "permission[]");
    }
}
