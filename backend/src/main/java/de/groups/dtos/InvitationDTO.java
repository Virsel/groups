package de.groups.dtos;

import de.groups.models.GroupEntryRequestEnum;

import java.sql.Timestamp;

public class InvitationDTO extends GroupEntryRequestDTO {
    private GroupMemberDTO issuer;

    private String relatedUserName;

    public InvitationDTO() {
    }

    public InvitationDTO(String id, Timestamp createdAt, String relatedGroupName, GroupEntryRequestEnum status, GroupMemberDTO issuer, String relatedUserName) {
        super(id, createdAt, relatedGroupName, status);
        this.issuer = issuer;
        this.relatedUserName = relatedUserName;
    }

    public GroupMemberDTO getIssuer() {
        return issuer;
    }

    public void setIssuer(GroupMemberDTO issuer) {
        this.issuer = issuer;
    }

    public String getRelatedUserName() {
        return relatedUserName;
    }

    public void setRelatedUserName(String relatedUserName) {
        this.relatedUserName = relatedUserName;
    }
}
