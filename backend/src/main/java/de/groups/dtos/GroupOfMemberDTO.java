package de.groups.dtos;

import de.groups.models.MemberRole;

import java.util.List;

public class GroupOfMemberDTO {
    private String groupId;

    private String groupRepresantationId;

    private String name;

    private String category;

    private String description;

    private List<MemberRole> roles;

    public GroupOfMemberDTO() {
    }

    public GroupOfMemberDTO(String groupId, String groupRepresantationId, String name, String category, String description) {
        this.groupId = groupId;
        this.groupRepresantationId = groupRepresantationId;
        this.name = name;
        this.category = category;
        this.description = description;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupRepresantationId() {
        return groupRepresantationId;
    }

    public void setGroupRepresantationId(String groupRepresantationId) {
        this.groupRepresantationId = groupRepresantationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MemberRole> getRoles() {
        return roles;
    }

    public void setRoles(List<MemberRole> roles) {
        this.roles = roles;
    }
}
