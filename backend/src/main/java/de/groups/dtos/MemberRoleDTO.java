package de.groups.dtos;

import de.groups.models.MemberRole;

import java.util.List;

public class MemberRoleDTO {

    List<MemberRole> memberRoleList;

    public MemberRoleDTO() {
    }

    public MemberRoleDTO(List<MemberRole> memberRoleList) {
        this.memberRoleList = memberRoleList;
    }

    public List<MemberRole> getMemberRoleList() {
        return memberRoleList;
    }

    public void setMemberRoleList(List<MemberRole> memberRoleList) {
        this.memberRoleList = memberRoleList;
    }
}
