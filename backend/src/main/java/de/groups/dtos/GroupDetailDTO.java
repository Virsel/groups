package de.groups.dtos;

import java.sql.Timestamp;
import java.util.List;

public class GroupDetailDTO {
    private String groupId;

    private String name;

    private Timestamp createdAt;

    private String category;

    private String description;

    private String founderName;

    private List<GroupMemberDTO> members;

    private List<PieceOfNewsDTO> news;

    public GroupDetailDTO() {
    }

    public GroupDetailDTO(String name) {
        this.name = name;
    }

    public GroupDetailDTO(String groupId, String name, java.util.Date createdAt, String category, String description, String founderName) {
        this.groupId = groupId;
        this.name = name;
        this.createdAt = Timestamp.from(createdAt.toInstant());
        this.category = category;
        this.description = description;
        this.founderName = founderName;
    }

    public GroupDetailDTO(String groupId, String name, Timestamp createdAt, String category, String description, String founderName, List<GroupMemberDTO> members, List<PieceOfNewsDTO> news) {
        this.groupId = groupId;
        this.name = name;
        this.createdAt = createdAt;
        this.category = category;
        this.description = description;
        this.founderName = founderName;
        this.members = members;
        this.news = news;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getFounderName() {
        return founderName;
    }

    public void setFounderName(String founderName) {
        this.founderName = founderName;
    }

    public List<GroupMemberDTO> getMembers() {
        return members;
    }

    public void setMembers(List<GroupMemberDTO> members) {
        this.members = members;
    }

    public List<PieceOfNewsDTO> getNews() {
        return news;
    }

    public void setNews(List<PieceOfNewsDTO> news) {
        this.news = news;
    }
}
