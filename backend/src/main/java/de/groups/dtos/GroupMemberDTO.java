package de.groups.dtos;

import de.groups.models.MemberRole;

import java.util.List;


public class GroupMemberDTO {
    private String userId;

    public String getGroupMemberId() {
        return groupMemberId;
    }

    public void setGroupMemberId(String groupMemberId) {
        this.groupMemberId = groupMemberId;
    }

    private String groupMemberId;

    private String name;

    public List<MemberRole> getRoles() {
        return roles;
    }

    public void setRoles(List<MemberRole> roles) {
        this.roles = roles;
    }

    private List<MemberRole> roles;

    public GroupMemberDTO() {
    }

    public GroupMemberDTO(String userId, String groupMemberId, String name, List<MemberRole> roles) {
        this.userId = userId;
        this.groupMemberId = groupMemberId;
        this.name = name;
        this.roles = roles;
    }

    public GroupMemberDTO(String userId, String groupMemberId, String name) {
        this.userId = userId;
        this.groupMemberId = groupMemberId;
        this.name = name;
    }

    public GroupMemberDTO(String userId, String name, List<MemberRole> roles) {
        this.userId = userId;
        this.name = name;
        this.roles = roles;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
