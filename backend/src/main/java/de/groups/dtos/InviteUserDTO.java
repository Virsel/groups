package de.groups.dtos;

public class InviteUserDTO {

    private String userId;
    private String issuerId;

    public InviteUserDTO() {
    }

    public InviteUserDTO(String userId, String issuerId) {
        this.userId = userId;
        this.issuerId = issuerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIssuerId() {
        return issuerId;
    }

    public void setIssuerId(String issuerId) {
        this.issuerId = issuerId;
    }
}
