package de.groups.dtos;

import java.sql.Timestamp;
import java.util.List;

public class GroupDTO {
    private String groupId;

    private String name;

    private String category;

    private String description;

    public GroupDTO(String groupId, String name, String category, String description) {
        this.groupId = groupId;
        this.name = name;
        this.category = category;
        this.description = description;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
