package de.groups.dtos;

import de.groups.models.GroupEntryRequestEnum;

import java.sql.Timestamp;

public class GroupEntryRequestDTO {
    private String id;

    private Timestamp createdAt;

    private String relatedGroupName;

    public GroupEntryRequestEnum getStatus() {
        return status;
    }

    public void setStatus(GroupEntryRequestEnum status) {
        this.status = status;
    }

    private GroupEntryRequestEnum status;

    public GroupEntryRequestDTO() {
    }

    public GroupEntryRequestDTO(String id, java.util.Date createdAt, String relatedGroupName, GroupEntryRequestEnum status) {
        this.id = id;
        this.createdAt = Timestamp.from(createdAt.toInstant());
        this.relatedGroupName = relatedGroupName;
        this.status = status;
    }

    public String getGroupEntryRequestId() {
        return id;
    }

    public void setGroupEntryRequestId(String groupEntryRequestId) {
        this.id = groupEntryRequestId;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getRelatedGroupName() {
        return relatedGroupName;
    }

    public void setRelatedGroupName(String relatedGroupName) {
        this.relatedGroupName = relatedGroupName;
    }

}
