package de.groups.dtos;

import java.sql.Timestamp;
import java.util.Date;

public class PieceOfNewsDTO {
    private String newsId;

    private String title;

    private String content;

    private Timestamp createdAt;

    private String authorName;

    private String groupName;

    private String category;

    public PieceOfNewsDTO() {
    }

    public PieceOfNewsDTO(String newsId, String title, String content, Date createdAt, String authorName) {
        this.newsId = newsId;
        this.title = title;
        this.content = content;
        this.createdAt = new Timestamp(createdAt.getTime());
        this.authorName = authorName;
    }

    public PieceOfNewsDTO(String newsId, String title, String content, Timestamp createdAt, String authorName, String groupName, String category) {
        this.newsId = newsId;
        this.title = title;
        this.content = content;
        this.createdAt = createdAt;
        this.authorName = authorName;
        this.groupName = groupName;
        this.category = category;
    }

    public PieceOfNewsDTO(String newsId, String title, String content, Date createdAt, String authorName, String groupName, String category) {
        this.newsId = newsId;
        this.title = title;
        this.content = content;
        this.createdAt = new Timestamp(createdAt.getTime());
        this.authorName = authorName;
        this.groupName = groupName;
        this.category = category;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
