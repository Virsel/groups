package de.groups.responses;

import java.util.ArrayList;
import java.util.List;

public class BaseResponse {
    private List<String> messages = new ArrayList<>();

    private List<String> errors = new ArrayList<>();

    public BaseResponse() {
    }

    public BaseResponse(List<String> messages, List<String> errors) {
        this.messages = messages;
        this.errors = errors;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
    public void addError(String error) {
        this.errors.add(error);
    }
    public void addMsg(String error) {
        this.messages.add(error);
    }
}
