package de.groups.responses;

import java.util.List;

public class DataResponse<T> extends BaseResponse{
    private T data;

    public DataResponse() {
    }

    public DataResponse(List<String> messages, List<String> errors, T data) {
        super(messages, errors);
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
