package de.groups.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
public class UserEntryRequest extends GroupEntryRequest {

    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(CascadeType.SAVE_UPDATE)
    private User issuer;

    public UserEntryRequest() {
        this.issuer = new User();
    }

    public UserEntryRequest(String groupEntryRequestId, Timestamp createdAt, Group relatedGroup, boolean accepted, User issuer) {
        super(groupEntryRequestId, createdAt, relatedGroup, accepted);
        this.issuer = issuer;
    }

    public User getIssuer() {
        return issuer;
    }

    public void setIssuer(User issuer) {
        this.issuer = issuer;
    }

}
