package de.groups.models;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "group_table")
public class Group {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String groupId;

    @Column(unique = true)
    private String name;

    private Timestamp createdAt;

    private String category;

    @Column(columnDefinition="TEXT", length = 2048)
    private String description;

    @ManyToOne(cascade=CascadeType.DETACH)
    @LazyCollection(LazyCollectionOption.FALSE)
    private User founder;

    @OneToMany(cascade=CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<GroupMember> members;

    public Group() {
        this.groupId = "";
        this.name = "";
        this.createdAt = new Timestamp(0);
        this.category = "";
        this.description = "";
        this.founder = new User();
        this.members = new ArrayList<>();
    }

    public Group(String name, Timestamp timestamp, String category, String description) {
        this.groupId = "";
        this.name = name;
        this.createdAt = timestamp;
        this.category = category;
        this.description = description;
        this.members = new ArrayList<>();
    }

    public Group(String name, Timestamp timestamp, String category, String description, User founder) {
        this.groupId = "";
        this.name = name;
        this.createdAt = timestamp;
        this.category = category;
        this.description = description;
        this.founder = founder;
        this.members = new ArrayList<>();
    }

    public Group(String groupId, String name, Timestamp createdAt, String category, String description, User founder, List<GroupMember> members) {
        this.groupId = groupId;
        this.name = name;
        this.createdAt = createdAt;
        this.category = category;
        this.description = description;
        this.founder = founder;
        this.members = members;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getFounder() {
        return founder;
    }

    public void setFounder(User founder) {
        this.founder = founder;
    }

    public List<GroupMember> getMembers() {
        return members;
    }

    public void setMembers(List<GroupMember> members) {
        this.members = members;
    }

}
