package de.groups.models;

import com.vladmihalcea.hibernate.type.array.EnumArrayType;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

@Entity
@TypeDef(typeClass = EnumArrayType.class, defaultForType = Permission[].class, parameters = {@Parameter(name = EnumArrayType.SQL_ARRAY_TYPE, value = "permission")})
public class MemberRole {


    @Id
    @Column(unique = true)
    private String name;

    @Column(columnDefinition = "permission[]")
    private Permission[] permissions;

    public MemberRole() {
        this.name = "";
        this.permissions = new Permission[]{};
    }

    public MemberRole(String name, Permission[] permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    public MemberRole(String memberRoleId, String name, Permission[] permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Permission[] getPermissions() {
        return permissions;
    }

    public void setPermissions(Permission[] permissions) {
        this.permissions = permissions;
    }

    public static MemberRole getStandardRole(){
        return new MemberRole("Standard", Permission.getStandardPermissions());
    }

    public static MemberRole getModeratorRole(){
        return new MemberRole("Moderator", Permission.getModeratorPermissions());
    }

    public static MemberRole getAdminRole(){
        return new MemberRole("Admin", Permission.getAdminPermissions());
    }

    public static MemberRole getRole(String name){
        switch (name) {
            case "Admin":
                return getAdminRole();
            case "Moderator":
                return getModeratorRole();
            case "Standard":
                return getStandardRole();
            default:
                return getStandardRole();
        }
    }

    public static List<MemberRole> getRoles(List<String> names){
        List<MemberRole> res = new ArrayList<>();
        for (String role : names) {
            res.add(MemberRole.getRole(role));
        }
        return res;
    }
}
