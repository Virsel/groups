package de.groups.models;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class PieceOfNews {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String newsId;

    private String title;

    @Column(columnDefinition="TEXT", length = 2048)
    private String content;

    private Timestamp createdAt;

    @ManyToOne
    private Group relatedGroup;

    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    private GroupMember creator;

    public PieceOfNews() {
        this.newsId = "";
        this.title = "";
        this.content = "";
        this.createdAt = new Timestamp(0);
        this.relatedGroup = new Group();
        this.creator = new GroupMember();
    }

    public PieceOfNews(String newsId, String title, String content, java.util.Date createdAt, GroupMember creator) {
        this.newsId = newsId;
        this.title = title;
        this.content = content;
        this.createdAt = Timestamp.from(createdAt.toInstant());
        this.creator = creator;
    }

    public PieceOfNews(String title, String content, Timestamp timestamp, Group relatedGroup, GroupMember creator) {
        this.newsId = "";
        this.title = title;
        this.content = content;
        this.createdAt = timestamp;
        this.relatedGroup = relatedGroup;
        this.creator = creator;
    }

    public PieceOfNews(String newsId, String title, String content, Timestamp createdAt, Group relatedGroup, GroupMember creator) {
        this.newsId = newsId;
        this.title = title;
        this.content = content;
        this.createdAt = createdAt;
        this.relatedGroup = relatedGroup;
        this.creator = creator;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Group getRelatedGroup() {
        return relatedGroup;
    }

    public void setRelatedGroup(Group relatedGroup) {
        this.relatedGroup = relatedGroup;
    }

    public GroupMember getCreator() {
        return creator;
    }

    public void setCreator(GroupMember creator) {
        this.creator = creator;
    }

}
