package de.groups.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_table")
public class User implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String userId;

    @Column(unique = true)
    private String name;

    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<GroupMember> groupRepresentations;

    public User() {
        this.userId = "";
        this.name = "";
        this.groupRepresentations = new ArrayList<>();
    }

    public User(String name) {
        this.userId = "";
        this.name = name;
        this.groupRepresentations = new ArrayList<>();
    }

    public User(String userId, String name, List<GroupMember> groupRepresentations) {
        this.userId = userId;
        this.name = name;
        this.groupRepresentations = groupRepresentations;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GroupMember> getGroupRepresentations() {
        return groupRepresentations;
    }

    public void setGroupRepresentations(List<GroupMember> groupRepresentations) {
        this.groupRepresentations = groupRepresentations;
    }

    public void addGroupRepresentation(GroupMember groupRepresentation) {
        this.groupRepresentations.add(groupRepresentation);
    }
}
