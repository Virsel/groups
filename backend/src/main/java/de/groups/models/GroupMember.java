package de.groups.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class GroupMember {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String groupMemberId;

    private Boolean deleted = false;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "set_roles", joinColumns = @JoinColumn(name = "groupMemberId"), inverseJoinColumns = @JoinColumn(name = "memberRoleId"))
    private List<MemberRole> roles;

    @ManyToOne(cascade = CascadeType.ALL)
    private User user;

    @ManyToOne(cascade = CascadeType.DETACH)
    private Group group;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }


    public GroupMember() {
        this.groupMemberId = "";
        this.roles = new ArrayList<>();
    }

    public GroupMember(String groupMemberId, List<MemberRole> roles) {
        this.groupMemberId = groupMemberId;
        this.roles = roles;
    }

    public String getGroupMemberId() {
        return groupMemberId;
    }

    public void setGroupMemberId(String groupMemberId) {
        this.groupMemberId = groupMemberId;
    }

    public List<MemberRole> getRoles() {
        return roles;
    }

    public void setRoles(List<MemberRole> roles) {
        this.roles = roles;
    }

    public void addRole(MemberRole role) {
        this.roles.add(role);
    }

    public boolean isAdmin(){
        return this.roles.stream().anyMatch(x -> x.getName() == "Admin");
    }

    public boolean isModerator(){
        return this.roles.stream().anyMatch(x -> x.getName() == "Moderator");
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
