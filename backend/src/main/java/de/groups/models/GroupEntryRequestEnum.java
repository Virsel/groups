package de.groups.models;

public enum GroupEntryRequestEnum {
    ACCEPTED,
    DENIED,
    IN_PROGRESS,
}
