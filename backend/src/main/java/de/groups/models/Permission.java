package de.groups.models;

public enum Permission {
    DELETE_GROUP,
    INVITE_MEMBER,
    EDIT_GROUP_ENTRY_REQUEST,
    REMOVE_MEMBER,
    CREATE_ROLE,
    DELETE_ROLE,
    ADD_ROLE_TO_MEMBER,
    REMOVE_ROLE_FROM_MEMBER,
    CREATE_PIECE_OF_NEWS,
    DELETE_PIECE_OF_NEWS,
    EDIT_PIECE_OF_NEWS,
    DELETE_OWN_PIECE_OF_NEWS,
    EDIT_OWN_PIECE_OF_NEWS;


    public static Permission[] getStandardPermissions() {
        return new Permission[]{
                Permission.CREATE_PIECE_OF_NEWS,
                Permission.DELETE_OWN_PIECE_OF_NEWS,
                Permission.EDIT_OWN_PIECE_OF_NEWS};
    }

    public static Permission[] getModeratorPermissions() {
        return new Permission[]{
                Permission.CREATE_PIECE_OF_NEWS,
                Permission.DELETE_OWN_PIECE_OF_NEWS,
                Permission.EDIT_OWN_PIECE_OF_NEWS,
                Permission.DELETE_PIECE_OF_NEWS,
                Permission.EDIT_PIECE_OF_NEWS};
    }


    public static Permission[] getAdminPermissions() {
        return new Permission[]{
                Permission.DELETE_GROUP,
                Permission.INVITE_MEMBER,
                Permission.EDIT_GROUP_ENTRY_REQUEST,
                Permission.REMOVE_MEMBER,
                Permission.CREATE_ROLE,
                Permission.DELETE_ROLE,
                Permission.ADD_ROLE_TO_MEMBER,
                Permission.REMOVE_ROLE_FROM_MEMBER,
                Permission.CREATE_PIECE_OF_NEWS,
                Permission.DELETE_PIECE_OF_NEWS,
                Permission.EDIT_OWN_PIECE_OF_NEWS,
                Permission.DELETE_OWN_PIECE_OF_NEWS,
                Permission.EDIT_PIECE_OF_NEWS};
    }
}
