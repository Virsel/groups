package de.groups.models;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class GroupEntryRequest {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String groupEntryRequestId;

    private Timestamp createdAt;

    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    private Group relatedGroup;

    public GroupEntryRequestEnum getStatus() {
        return status;
    }

    public void setStatus(GroupEntryRequestEnum status) {
        this.status = status;
    }

    private GroupEntryRequestEnum status;

    public GroupEntryRequest() {
        this.groupEntryRequestId = "";
        this.createdAt = new Timestamp(0);
        this.relatedGroup = new Group();
        this.status = GroupEntryRequestEnum.IN_PROGRESS;
    }

    public GroupEntryRequest(String groupEntryRequestId, Timestamp createdAt, Group relatedGroup, boolean accepted) {
        this.groupEntryRequestId = groupEntryRequestId;
        this.createdAt = createdAt;
        this.relatedGroup = relatedGroup;
        this.status = GroupEntryRequestEnum.IN_PROGRESS;
    }

    public String getGroupEntryRequestId() {
        return groupEntryRequestId;
    }

    public void setGroupEntryRequestId(String groupEntryRequestId) {
        this.groupEntryRequestId = groupEntryRequestId;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Group getRelatedGroup() {
        return relatedGroup;
    }

    public void setRelatedGroup(Group relatedGroup) {
        this.relatedGroup = relatedGroup;
    }
}
