package de.groups.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
public class Invitation extends GroupEntryRequest {

    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    private GroupMember issuer;

    @ManyToOne
    @LazyCollection(LazyCollectionOption.FALSE)
    @Cascade(CascadeType.SAVE_UPDATE)
    private User relatedUser;


    public Invitation() {
        this.issuer = new GroupMember();
        this.relatedUser = new User();
    }

    public Invitation(GroupMember issuer, User relatedUser) {
        super();
        this.issuer = issuer;
        this.relatedUser = relatedUser;
    }

    public Invitation(String groupEntryRequestId, Timestamp createdAt, Group relatedGroup, boolean accepted, GroupMember issuer, User relatedUser) {
        super(groupEntryRequestId, createdAt, relatedGroup, accepted);
        this.issuer = issuer;
        this.relatedUser = relatedUser;
    }

    public GroupMember getIssuer() {
        return issuer;
    }

    public void setIssuer(GroupMember issuer) {
        this.issuer = issuer;
    }

    public User getRelatedUser() {
        return relatedUser;
    }

    public void setRelatedUser(User relatedUser) {
        this.relatedUser = relatedUser;
    }

}
