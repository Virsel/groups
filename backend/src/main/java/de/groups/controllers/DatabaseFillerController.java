package de.groups.controllers;

import de.groups.helpers.seeder.DatabaseFiller;
import de.groups.responses.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin
@RequestMapping("/api/db")
public class DatabaseFillerController {

    @Autowired
    private DatabaseFiller databaseFiller;

    @GetMapping("/fill")
    public ResponseEntity<BaseResponse> fillDatabaseWithMockupData() {
        BaseResponse baseResponse = new BaseResponse();

        databaseFiller.fillDatabase();

        ArrayList<String> messages = new ArrayList<>();
        messages.add("successfully populated the database with records");
        baseResponse.setMessages(messages);

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @DeleteMapping("/deleteall")
    public ResponseEntity<BaseResponse> deleteDatabaseMockupData() {
        BaseResponse baseResponse = new BaseResponse();

        databaseFiller.deleteDatabaseRecords();

        ArrayList<String> messages = new ArrayList<>();
        messages.add("successfully deleted all database records");
        baseResponse.setMessages(messages);

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
}
