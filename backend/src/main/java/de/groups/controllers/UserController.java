package de.groups.controllers;

import de.groups.dtos.GroupMemberDTO;
import de.groups.dtos.GroupOfMemberDTO;
import de.groups.dtos.InvitationDTO;
import de.groups.dtos.PieceOfNewsDTO;
import de.groups.models.*;
import de.groups.responses.BaseResponse;
import de.groups.responses.DataResponse;
import de.groups.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private GroupMemberService groupMemberService;

    @GetMapping("groups")
    public ResponseEntity<DataResponse<List<GroupOfMemberDTO>>> findAllGroups(@RequestHeader HttpHeaders headers) {
        DataResponse<List<GroupOfMemberDTO>> dataResponse = new DataResponse<>();
        ArrayList<GroupOfMemberDTO> data = new ArrayList<>();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            if (!userService.existsByName(usernameFromToken)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            List<String> groupRepIds = groupMemberService.getGroupMemberIdsByUserName(usernameFromToken);

            for (String groupRepId : groupRepIds) {
                var group = groupService.findGroupForGroupMember(groupRepId);
                data.add(group);
            }

        } catch (Exception e) {
            dataResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        dataResponse.setData(data);

        return new ResponseEntity(dataResponse, HttpStatus.OK);
    }

    @GetMapping("invitations")
    public ResponseEntity<DataResponse<List<InvitationDTO>>> findAllInvitations(@RequestHeader HttpHeaders headers) {

        DataResponse<List<InvitationDTO>> dataResponse = new DataResponse<>();
        ArrayList<String> messages = new ArrayList<>();
        ArrayList<String> errors = new ArrayList<>();
        ArrayList<InvitationDTO> data = new ArrayList<>();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUser = userService.findByName(usernameFromToken);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            List<Invitation> invitations = invitationService.findAllByRelatedUser(user);
            List<InvitationDTO> invitationDTOS = new ArrayList<>();

            for (Invitation invitation : invitations) {
                String groupEntryRequestId = invitation.getGroupEntryRequestId();
                Timestamp createdAt = invitation.getCreatedAt();
                String relatedGroupName = invitation.getRelatedGroup().getName();

                String relatedUsername = invitation.getRelatedUser().getName();

                String userId = invitation.getIssuer().getUser().getUserId();
                String groupMemberId = invitation.getIssuer().getGroupMemberId();
                String issuerName = invitation.getIssuer().getUser().getName();
                List<MemberRole> roles = invitation.getIssuer().getRoles();

                GroupMemberDTO issuer = new GroupMemberDTO(userId, groupMemberId, issuerName, roles);

                InvitationDTO invitationDTO = new InvitationDTO(groupEntryRequestId, createdAt, relatedGroupName, invitation.getStatus(), issuer, relatedUsername);
                invitationDTOS.add(invitationDTO);
            }

            data.addAll(invitationDTOS);

            messages.add("successfully fetched all invitations for user " + user.getName());

        } catch (Exception e) {
            errors.add(e.getMessage());
        }

        dataResponse.setMessages(messages);
        dataResponse.setErrors(errors);
        dataResponse.setData(data);

        return new ResponseEntity<>(dataResponse, HttpStatus.OK);
    }

    @GetMapping("news")
    public ResponseEntity<DataResponse<List<PieceOfNewsDTO>>> findAllNews(@RequestHeader HttpHeaders headers) {

        DataResponse<List<PieceOfNewsDTO>> dataResponse = new DataResponse<>();
        ArrayList<String> messages = new ArrayList<>();
        ArrayList<String> errors = new ArrayList<>();
        ArrayList<PieceOfNewsDTO> data = new ArrayList<>();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUser = userService.findByName(usernameFromToken);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            List<GroupMember> groupRepresentations = user.getGroupRepresentations();

            List<PieceOfNewsDTO> news = new ArrayList<>();

            for (GroupMember groupMember : groupRepresentations) {

                Group group = groupMember.getGroup();

                List<PieceOfNewsDTO> newsDTOS = pieceOfNewsService.findByGroup(group.getGroupId());

                for (PieceOfNewsDTO newsDTO : newsDTOS) {
                    newsDTO.setGroupName(group.getName());
                    newsDTO.setCategory(group.getCategory());
                }

                news.addAll(newsDTOS);
            }

            data.addAll(news);

        } catch (Exception e) {
            errors.add("Es ist ein unbekannter Fehler aufgetreten!");
        }

        dataResponse.setMessages(messages);
        dataResponse.setErrors(errors);
        dataResponse.setData(data);

        return new ResponseEntity<>(dataResponse, HttpStatus.OK);
    }
}
