package de.groups.controllers;

import de.groups.dtos.GroupMemberDTO;
import de.groups.dtos.GroupOfMemberDTO;
import de.groups.dtos.InvitationDTO;
import de.groups.dtos.PieceOfNewsDTO;
import de.groups.models.*;
import de.groups.responses.BaseResponse;
import de.groups.responses.DataResponse;
import de.groups.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/group-member")
public class GroupMemberController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private GroupMemberService groupMemberService;


    @DeleteMapping("leave/{groupId}")
    public ResponseEntity<BaseResponse> leaveGroup(@RequestHeader HttpHeaders headers, @PathVariable String groupId) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            String groupRepresantationIdOpt = groupMemberService.getGroupMemberIdByGroupIdAndMemberName(groupId, usernameFromToken);

            if (groupRepresantationIdOpt.isEmpty()) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            groupMemberService.setDeletedById(true, groupRepresantationIdOpt);

            baseResponse.addMsg("Du bist erfolgreich ausgetreten.");

        } catch (Exception e) {
            baseResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity(baseResponse, HttpStatus.OK);
    }
}
