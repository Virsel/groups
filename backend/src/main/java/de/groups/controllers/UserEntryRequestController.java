package de.groups.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.groups.dtos.*;
import de.groups.models.*;
import de.groups.responses.DataResponse;
import de.groups.services.GroupService;
import de.groups.services.UserEntryRequestService;
import de.groups.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/user-entry-requests")
public class UserEntryRequestController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserEntryRequestService userEntryRequestService;

    @PostMapping
    public ResponseEntity create(@RequestBody ObjectNode jsonNodes) {

        DataResponse<UserEntryRequest> dataResponse = new DataResponse<>();
        ArrayList<String> messages = new ArrayList<>();
        ArrayList<String> errors = new ArrayList<>();

        String issuerName = jsonNodes.get("IssuerName").asText();
        String relatedGroupName = jsonNodes.get("RelatedGroupName").asText();

        try {
            Optional<User> optionalUser = userService.findByName(issuerName);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            Optional<GroupDTO> optionalGroup = groupService.findByName(relatedGroupName);

            if (optionalGroup.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            GroupDTO groupDTO = optionalGroup.get();

            UserEntryRequest userEntryRequest = new UserEntryRequest();

            Timestamp timestamp = new Timestamp(new Date().getTime());
            userEntryRequest.setCreatedAt(timestamp);
            userEntryRequest.setIssuer(user);
            Group group = new Group();
            group.setGroupId(groupDTO.getGroupId());
            userEntryRequest.setRelatedGroup(group);
            userEntryRequest.setStatus(GroupEntryRequestEnum.IN_PROGRESS);

            userEntryRequestService.save(userEntryRequest);

            messages.add("Anfrage wurde erfolgreich erstellt.");

        } catch (Exception e) {
            errors.add(e.getMessage());
        }

        dataResponse.setMessages(messages);
        dataResponse.setErrors(errors);

        return new ResponseEntity<>(dataResponse, HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity findAllByUser(@RequestHeader HttpHeaders headers) {
        DataResponse<List<GroupEntryRequestDTO>> dataResponse = new DataResponse<>();
        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            String username = accessTokens.get(0);

            if (!userService.existsByName(username)) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            var userIdOpt = userService.getIdByName(username);
            User user = new User();
            user.setUserId(userIdOpt.get());

            var res = userEntryRequestService.getAllByIssuer(user);
            dataResponse.setData(res);
        } catch (Exception e) {
            dataResponse.addError("Ein unbekannter Fehler ist aufgetreten.");
        }

        return new ResponseEntity(dataResponse, HttpStatus.OK);
    }
}
