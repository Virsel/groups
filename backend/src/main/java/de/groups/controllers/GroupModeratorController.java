package de.groups.controllers;

import de.groups.models.*;
import de.groups.responses.BaseResponse;
import de.groups.services.PieceOfNewsService;
import de.groups.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/group-moderator")
public class GroupModeratorController {

    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private UserService userService;

    @DeleteMapping("news/{pieceOfNewsID}")
    public ResponseEntity<BaseResponse> deleteForeignNews(@RequestHeader HttpHeaders headers, @PathVariable String pieceOfNewsID) {
        BaseResponse baseResponse = new BaseResponse();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUser = userService.findByName(usernameFromToken);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            List<GroupMember> groupRepresentations = user.getGroupRepresentations();

            Optional<PieceOfNews> optionalPieceOfNews = pieceOfNewsService.findById(pieceOfNewsID);

            if (optionalPieceOfNews.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            PieceOfNews pieceOfNews = optionalPieceOfNews.get();

            String relatedGroupID = pieceOfNews.getRelatedGroup().getGroupId();

            boolean isGroupModerator = false;

            for (GroupMember groupMember : groupRepresentations) {
                String groupID = groupMember.getGroup().getGroupId();

                if (Objects.equals(groupID, relatedGroupID)) {
                    List<MemberRole> roles = groupMember.getRoles();

                    for (MemberRole role : roles) {
                        if (role.getName().equals("Admin") || role.getName().equals("Moderator")) {
                            isGroupModerator = true;
                            break;
                        }
                    }
                }
            }

            if (!isGroupModerator) {
                baseResponse.addError("Du bist nicht athoriziert.");
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            pieceOfNewsService.delete(pieceOfNews);
            baseResponse.addMsg("Nachricht erfolgreich gelöscht.");

        } catch (Exception e) {
            baseResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }
}
