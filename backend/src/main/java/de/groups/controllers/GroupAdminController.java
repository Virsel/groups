package de.groups.controllers;

import de.groups.dtos.ForeignUserDTO;
import de.groups.dtos.InviteUserDTO;
import de.groups.dtos.MemberRoleDTO;
import de.groups.models.*;
import de.groups.responses.BaseResponse;
import de.groups.responses.DataResponse;
import de.groups.services.*;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api/group-admin")
public class GroupAdminController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private GroupMemberService groupMemberService;

    private SessionFactory hibernateFactory;

    @Autowired
    public GroupAdminController(EntityManagerFactory factory) {
        if (factory.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        this.hibernateFactory = factory.unwrap(SessionFactory.class);
    }

    @GetMapping("groups/{groupName}/users-for-invitation")
    public ResponseEntity getForeignUsersForInvitation(@RequestHeader HttpHeaders headers, @PathVariable String groupName) {
        DataResponse dataResponse = new DataResponse<>();
        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            var res = userService.getUsersNotInGroup(groupName);

            for (ForeignUserDTO foreignUserDTO : res
            ) {
                var newsCount = pieceOfNewsService.getCountByUserName(foreignUserDTO.getUserName());
                foreignUserDTO.setNewsCount(newsCount);
            }

            dataResponse.setData(res);
        } catch (Exception e) {
            dataResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity(dataResponse, HttpStatus.OK);
    }

    @DeleteMapping("groups/{groupID}")
    public ResponseEntity<BaseResponse> deleteGroup(@RequestHeader HttpHeaders headers, @PathVariable String groupID) {
        BaseResponse baseResponse = new BaseResponse();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUser = userService.findByName(usernameFromToken);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            Optional<Group> optionalGroup = groupService.findById(groupID);

            if (optionalGroup.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            Group relatedGroup = optionalGroup.get();

            List<GroupMember> groupRepresentations = user.getGroupRepresentations();

            boolean isAdminInGroup = false;

            for (GroupMember groupMember : groupRepresentations) {
                Group group = groupMember.getGroup();

                if (Objects.equals(group.getGroupId(), relatedGroup.getGroupId())) {
                    List<MemberRole> roles = groupMember.getRoles();

                    for (MemberRole role : roles) {
                        if (Objects.equals(role, MemberRole.getAdminRole())) {
                            isAdminInGroup = true;
                            break;
                        }
                    }
                    break;
                }
            }

            if (!isAdminInGroup) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            groupService.delete(relatedGroup);
            baseResponse.addMsg("Gruppe erfolgreich gelöscht.");

        } catch (Exception e) {
            baseResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @DeleteMapping("members/{groupMemberID}")
    public ResponseEntity<BaseResponse> deleteGroupMember(@RequestHeader HttpHeaders headers, @PathVariable String groupMemberID) {
        BaseResponse baseResponse = new BaseResponse();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUser = userService.findByName(usernameFromToken);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            Optional<GroupMember> optionalGroupMember = groupMemberService.findById(groupMemberID);

            if (optionalGroupMember.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            GroupMember relatedGroupMember = optionalGroupMember.get();

            Group relatedGroup = relatedGroupMember.getGroup();

            List<GroupMember> groupRepresentations = user.getGroupRepresentations();

            boolean isAdminInGroup = false;

            for (GroupMember groupMember : groupRepresentations) {
                Group group = groupMember.getGroup();

                if (Objects.equals(group.getGroupId(), relatedGroup.getGroupId())) {
                    List<MemberRole> roles = groupMember.getRoles();

                    for (MemberRole role : roles) {
                        if (role.getName().equals("Admin")) {
                            isAdminInGroup = true;
                            break;
                        }
                    }
                    break;
                }
            }

            if (!isAdminInGroup) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            groupMemberService.setDeletedById(true, relatedGroupMember.getGroupMemberId());
            baseResponse.addMsg("Nutzer erfolgreich gelöscht.");

        } catch (Exception e) {
            baseResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @PostMapping("members/{groupMemberID}/roles")
    public ResponseEntity<BaseResponse> editGroupMemberRoles(@RequestHeader HttpHeaders headers, @PathVariable String groupMemberID, @RequestBody List<String> requestedRoles) {
        BaseResponse baseResponse = new BaseResponse();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUser = userService.findByName(usernameFromToken);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            Optional<GroupMember> optionalGroupMember = groupMemberService.findById(groupMemberID);

            if (optionalGroupMember.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            GroupMember relatedGroupMember = optionalGroupMember.get();

            Group relatedGroup = relatedGroupMember.getGroup();

            List<GroupMember> groupRepresentations = user.getGroupRepresentations();

            boolean isAdminInGroup = false;

            for (GroupMember groupMember : groupRepresentations) {
                Group group = groupMember.getGroup();

                if (Objects.equals(group.getGroupId(), relatedGroup.getGroupId())) {
                    List<MemberRole> roles = groupMember.getRoles();

                    for (MemberRole role : roles) {
                        if (role.getName().equals("Admin")) {
                            isAdminInGroup = true;
                            break;
                        }
                    }
                    break;
                }
            }

            if (!isAdminInGroup) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            var session = this.hibernateFactory.openSession();
            var tx = session.getTransaction();
            tx.begin();

            GroupMember memberToBeEdited = session.load(GroupMember.class, groupMemberID);
            memberToBeEdited.setRoles(MemberRole.getRoles(requestedRoles));
            session.update(memberToBeEdited);

            tx.commit();
            session.close();

            baseResponse.addMsg("Rollen erfolgreich bearbeitet.");

        } catch (Exception e) {
            baseResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @PostMapping("invitations")
    public ResponseEntity<DataResponse> inviteGroupMember(@RequestHeader HttpHeaders headers, @RequestBody InviteUserDTO inviteUserDTO) {
        DataResponse<Invitation> dataResponse = new DataResponse<>();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUserIssuer = userService.findByName(usernameFromToken);

            if (optionalUserIssuer.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User userIssuer = optionalUserIssuer.get();

            String userToInviteID = inviteUserDTO.getUserId();

            Optional<User> optionalUserToInvite = userService.findByName(userToInviteID);

            if (optionalUserToInvite.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User userToInvite = optionalUserToInvite.get();

            String issuerGroupMemberID = inviteUserDTO.getIssuerId();

            Optional<GroupMember> optionalIssuer = groupMemberService.findById(issuerGroupMemberID);

            if (optionalIssuer.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            GroupMember relatedIssuer = optionalIssuer.get();

            Group relatedGroup = relatedIssuer.getGroup();

            List<GroupMember> groupRepresentations = userIssuer.getGroupRepresentations();

            boolean userIsIssuer = false;

            for (GroupMember groupMember : groupRepresentations) {
                if (groupMember == relatedIssuer) {
                    userIsIssuer = true;
                    break;
                }
            }

            if (!userIsIssuer) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            boolean isAdminInGroup = false;

            for (GroupMember groupMember : groupRepresentations) {
                Group group = groupMember.getGroup();

                if (Objects.equals(group.getGroupId(), relatedGroup.getGroupId())) {
                    List<MemberRole> roles = groupMember.getRoles();

                    for (MemberRole role : roles) {
                        if (Objects.equals(role, MemberRole.getAdminRole())) {
                            isAdminInGroup = true;
                            break;
                        }
                    }
                    break;
                }
            }

            if (!isAdminInGroup) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            Invitation invitation = new Invitation(relatedIssuer, userToInvite);

            dataResponse.setData(invitationService.save(invitation));
            dataResponse.addMsg("Einladung erfolgreich erstellt");

        } catch (Exception e) {
            dataResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity<>(dataResponse, HttpStatus.OK);
    }
}
