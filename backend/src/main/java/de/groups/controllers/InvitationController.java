package de.groups.controllers;

import de.groups.models.Invitation;
import de.groups.responses.DataResponse;
import de.groups.services.InvitationService;
import de.groups.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/invitations")
public class InvitationController {

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<DataResponse<List<Invitation>>> findAllInvitations() {

        DataResponse<List<Invitation>> dataResponse = new DataResponse<>();
        ArrayList<String> messages = new ArrayList<>();
        ArrayList<String> errors = new ArrayList<>();
        ArrayList<Invitation> data = new ArrayList<>();

        try {
            data.addAll(invitationService.findAll());
        } catch (Exception e) {
            errors.add(e.getMessage());
        }

        dataResponse.setMessages(messages);
        dataResponse.setErrors(errors);
        dataResponse.setData(data);

        return new ResponseEntity<>(dataResponse, HttpStatus.OK);
    }
}
