package de.groups.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.groups.dtos.GroupDTO;
import de.groups.dtos.GroupDetailDTO;
import de.groups.dtos.PieceOfNewsDTO;
import de.groups.models.*;
import de.groups.responses.BaseResponse;
import de.groups.responses.DataResponse;
import de.groups.services.GroupMemberService;
import de.groups.services.GroupService;
import de.groups.services.PieceOfNewsService;
import de.groups.services.UserService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api/news")
public class PieceOfNewsController {
    @Autowired
    private UserService userService;

    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private GroupMemberService groupMemberService;

    private SessionFactory hibernateFactory;

    @Autowired
    public PieceOfNewsController(EntityManagerFactory factory) {
        if (factory.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        this.hibernateFactory = factory.unwrap(SessionFactory.class);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody ObjectNode jsonNodes, @RequestHeader HttpHeaders headers) {
        BaseResponse baseResp = new BaseResponse();

        try {
            String groupMemberId = jsonNodes.get("GroupMemberId").asText();
            String title = jsonNodes.get("Title").asText();
            String content = jsonNodes.get("Content").asText();

            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            var userOpt = userService.findByName(usernameFromToken);
            if (userOpt.isEmpty()) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            var session = this.hibernateFactory.openSession();
            var tx = session.getTransaction();
            tx.begin();

            // create news
            var groupMember = new GroupMember();
            groupMember.setGroupMemberId(groupMemberId);
            var group = new Group();
            group.setGroupId(groupMemberService.getGroupId(groupMemberId));
            long time = new Date().getTime();
            Timestamp timestamp = new Timestamp(time);
            PieceOfNews pieceOfNews = new PieceOfNews(title, content, timestamp, group, groupMember);
            pieceOfNewsService.save(pieceOfNews);

            tx.commit();
            session.close();

            baseResp.addMsg("News wurde erfolgreich erstellt.");

        } catch (Exception e) {
            baseResp.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity(baseResp, HttpStatus.OK);
    }

    @DeleteMapping("{pieceOfNewsID}")
    public ResponseEntity<BaseResponse> deleteForeignNews(@RequestHeader HttpHeaders headers, @PathVariable String pieceOfNewsID) {
        BaseResponse baseResponse = new BaseResponse();

        try {
            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            Optional<User> optionalUser = userService.findByName(usernameFromToken);

            if (optionalUser.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            User user = optionalUser.get();

            List<GroupMember> groupRepresentations = user.getGroupRepresentations();

            Optional<PieceOfNews> optionalPieceOfNews = pieceOfNewsService.findById(pieceOfNewsID);

            if (optionalPieceOfNews.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            PieceOfNews pieceOfNews = optionalPieceOfNews.get();

            if (pieceOfNews.getCreator().getUser().getUserId() != user.getUserId()) {
                baseResponse.addError("Du bist nicht athoriziert.");
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            pieceOfNewsService.delete(pieceOfNews);

            baseResponse.addMsg("Nachricht erfolgreich gelöscht.");

        } catch (Exception e) {
            baseResponse.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        return new ResponseEntity<>(baseResponse, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<DataResponse<List<PieceOfNewsDTO>>> findAllNews() {
        DataResponse<List<PieceOfNewsDTO>> dataResponse = new DataResponse<>();
        try {
            List<PieceOfNewsDTO> newsDTOS = pieceOfNewsService.getAllAsFlatList();

            dataResponse.setData(newsDTOS);

        } catch (Exception e) {
            dataResponse.addError("Es ist ein unbekannter Fehler aufgetreten!");
        }

        return new ResponseEntity<>(dataResponse, HttpStatus.OK);
    }
}
