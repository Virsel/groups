package de.groups.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import de.groups.dtos.GroupDTO;
import de.groups.dtos.GroupDetailDTO;
import de.groups.dtos.GroupOfMemberDTO;
import de.groups.models.Group;
import de.groups.models.GroupMember;
import de.groups.models.MemberRole;
import de.groups.models.User;
import de.groups.responses.BaseResponse;
import de.groups.responses.DataResponse;
import de.groups.services.GroupMemberService;
import de.groups.services.GroupService;
import de.groups.services.PieceOfNewsService;
import de.groups.services.UserService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api/groups")
public class GroupController {
    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    @Autowired
    private PieceOfNewsService pieceOfNewsService;

    @Autowired
    private GroupMemberService groupMemberService;

    private SessionFactory hibernateFactory;

    @Autowired
    public GroupController(EntityManagerFactory factory) {
        if (factory.unwrap(SessionFactory.class) == null) {
            throw new NullPointerException("factory is not a hibernate factory");
        }
        this.hibernateFactory = factory.unwrap(SessionFactory.class);
    }

    @GetMapping
    public ResponseEntity<DataResponse<List<GroupDTO>>> findAllGroups() {
        DataResponse<List<GroupDTO>> dataResponse = new DataResponse<>();

        try {
            dataResponse.setData(groupService.findAll());
        } catch (Exception e) {
            dataResponse.addError(e.getMessage());
        }

        return new ResponseEntity(dataResponse, HttpStatus.OK);
    }

    @GetMapping("/{groupName}")
    public ResponseEntity<GroupDetailDTO> findGroupByName(@PathVariable String groupName) {
        DataResponse<GroupDetailDTO> dataResponse = new DataResponse();

        try {
            var groupOpt = groupService.findByName2(groupName);
            if (groupOpt.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            GroupDetailDTO groupDTO = groupOpt.get();
            var memberDTOList = groupMemberService.findByGroupId(groupDTO.getGroupId());
            groupDTO.setMembers(memberDTOList);
            var news = pieceOfNewsService.findByGroup(groupDTO.getGroupId());
            groupDTO.setNews(news);
//            var groupDTO = mapstructMapper.groupToGroupDTO(group);
//            groupDTO.setNews(news);
            dataResponse.setData(groupDTO);
        } catch (Exception e) {
            dataResponse.addError("Es ist ein unbekannter Fehler aufgetreten");
        }

        return new ResponseEntity(dataResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody ObjectNode jsonNodes, @RequestHeader HttpHeaders headers) {

        BaseResponse baseResp = new BaseResponse();
        ArrayList<String> messages = new ArrayList<>();
        ArrayList<String> errors = new ArrayList<>();

        try {
            String name = jsonNodes.get("Name").asText();
            String category = jsonNodes.get("Category").asText();
            String description = jsonNodes.get("Description").asText();

            List<String> accessTokens = headers.get("x-access-token");

            if (accessTokens == null || accessTokens.size() != 1) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            String usernameFromToken = accessTokens.get(0);

            var userOpt = userService.findByName(usernameFromToken);
            if (userOpt.isEmpty()) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            var user = new User();
            user.setUserId(userOpt.get().getUserId());

            var session = this.hibernateFactory.openSession();
            var tx = session.getTransaction();
            tx.begin();

            // create group
            Group group = new Group();
            Timestamp timestamp = new Timestamp(new Date().getTime());
            group.setCreatedAt(timestamp);
            group.setName(name);
            group.setCategory(category);
            group.setDescription(description);

            // create group repr
            var groupMember = new GroupMember();
            groupMember.setGroup(group);
            groupMember.addRole(MemberRole.getStandardRole());
            groupMember.addRole(MemberRole.getAdminRole());
            groupMember.setUser(user);
            session.saveOrUpdate(groupMember);

            user.addGroupRepresentation(groupMember);
            group.setMembers(Arrays.asList(groupMember));
            group.setFounder(user);
            session.saveOrUpdate(group);

            tx.commit();
            session.close();

            messages.add("Die Gruppe " + name + " wurde erfolgreich erstellt.");

        } catch (Exception e) {
            baseResp.addError("Es ist ein unbekannter Fehler aufgetreten.");
        }

        baseResp.setMessages(messages);

        return new ResponseEntity(baseResp, HttpStatus.OK);
    }
}
