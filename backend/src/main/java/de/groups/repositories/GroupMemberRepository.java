package de.groups.repositories;

import de.groups.dtos.GroupMemberDTO;
import de.groups.models.GroupMember;
import de.groups.models.MemberRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupMemberRepository extends JpaRepository<GroupMember, String> {

    @Modifying
    @Query("UPDATE GroupMember gm SET gm.deleted = ?1 WHERE gm.groupMemberId = ?2")
    void setGroupMemberDeletedById(boolean deleted, String id);

    @Modifying
    @Query("UPDATE GroupMember gm SET gm.roles = ?1 WHERE gm.groupMemberId = ?2")
    void setRolesById(List<MemberRole> memberRoleList, String id);

    @Query(value = "SELECT " +
            "new de.groups.dtos.GroupMemberDTO(" +
            "gm.user.userId, " +
            "gm.groupMemberId, " +
            "gm.user.name) FROM GroupMember gm WHERE gm.group.groupId =?1 AND gm.deleted = false ")
    List<GroupMemberDTO> findByGroupId(String groupId);

    @Query(value = "SELECT " +
            "gm.group.groupId FROM GroupMember gm WHERE gm.groupMemberId =?1 AND gm.deleted = false ")
    String getGroupId(String memberId);

    @Query(value = "SELECT gm.group_member_id FROM group_member gm, user_table u WHERE gm.group_group_id =:groupId AND gm.user_user_id = u.user_id AND u.name = :memberName AND NOT gm.deleted LIMIT 1", nativeQuery = true)
    String getGroupMemberIdByGroupIdAndMemberName(@Param("groupId") String groupId, @Param("memberName") String memberName);

    @Query(value = "SELECT r FROM GroupMember gm, MemberRole r WHERE gm.groupMemberId =?1 and r member gm.roles")
    List<MemberRole> getRoles(String groupMemberId);

    @Query(value = "SELECT new java.lang.String(gm.groupMemberId) FROM GroupMember gm WHERE gm.user.name =?1 and gm.deleted = false")
    List<String> getGroupMemberIdsByUserName(String username);
}
