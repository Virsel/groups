package de.groups.repositories;

import de.groups.models.MemberRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRoleRepository extends JpaRepository<MemberRole, String> {
    Optional<MemberRole> findByName(String strings);
}
