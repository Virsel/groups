package de.groups.repositories;

import de.groups.dtos.ForeignUserDTO;
import de.groups.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {
    Optional<User> findByName(String strings);
    boolean existsUserByName(String name);

    @Query(value = "SELECT u.userId FROM User u WHERE u.name =?1")
    Optional<String> getIdByName(String username);

    @Query(value = "SELECT new de.groups.dtos.ForeignUserDTO(u.userId, u.name) FROM User u, GroupMember gm WHERE gm.user.userId = u.userId AND NOT gm.group.name = ?1")
    List<ForeignUserDTO> getUsersNotInGroup(String groupName);
}
