package de.groups.repositories;

import de.groups.models.GroupEntryRequest;
import de.groups.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupEntryRequestRepository extends JpaRepository<GroupEntryRequest, String> {

}
