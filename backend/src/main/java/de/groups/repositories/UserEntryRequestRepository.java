package de.groups.repositories;

import de.groups.dtos.GroupEntryRequestDTO;
import de.groups.models.GroupEntryRequest;
import de.groups.models.User;
import de.groups.models.UserEntryRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserEntryRequestRepository extends JpaRepository<UserEntryRequest, String> {
    @Query(value = "SELECT " +
            "new de.groups.dtos.GroupEntryRequestDTO(" +
            "u_e_r.groupEntryRequestId, " +
            "u_e_r.createdAt, " +
            "u_e_r.relatedGroup.name, u_e_r.status) FROM UserEntryRequest u_e_r WHERE u_e_r.issuer =?1")
    List<GroupEntryRequestDTO> findByIssuer(User issuer);
}
