package de.groups.repositories;

import de.groups.dtos.GroupDTO;
import de.groups.dtos.GroupDetailDTO;
import de.groups.dtos.GroupOfMemberDTO;
import de.groups.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, String> {
    Optional<GroupDTO> findByName(String strings);

    @Query(value = "SELECT " +
            "new de.groups.dtos.GroupDTO(" +
            "g.groupId, " +
            "g.name, " +
            "g.category, " +
            "g.description) FROM Group g")
    List<GroupDTO> findAllAsDTO();

    @Query(value = "SELECT " +
            "new de.groups.dtos.GroupDetailDTO(" +
            "g.groupId, " +
            "g.name, " +
            "g.createdAt, " +
            "g.category, " +
            "g.description, " +
            "g.founder.name) FROM Group g WHERE g.name =?1")
    Optional<GroupDetailDTO> findByName2(String name);

    @Query(value = "SELECT " +
            "new de.groups.dtos.GroupOfMemberDTO(" +
            "g.groupId, gtm.groupMemberId, g.name, g.category, g.description) " +
            "FROM Group g, GroupMember gtm WHERE g.groupId = gtm.group.groupId and gtm.groupMemberId =?1")
    GroupOfMemberDTO findByGroupMemberId(@Param("groupMemberId") String groupMemberId);
}
