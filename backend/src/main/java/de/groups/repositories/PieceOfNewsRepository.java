package de.groups.repositories;


import de.groups.dtos.PieceOfNewsDTO;
import de.groups.models.Group;
import de.groups.models.PieceOfNews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PieceOfNewsRepository extends JpaRepository<PieceOfNews, String> {

    List<PieceOfNews> findByRelatedGroup(Group group);

    @Query(value = "SELECT NEW de.groups.models.PieceOfNews(n.newsId, n.title, n.content, n.createdAt, n.creator) from PieceOfNews n where n.relatedGroup = :group")
    List<PieceOfNews> getFlatListByRelatedGroup(@Param("group") Group group);

    @Query(value = "SELECT NEW de.groups.dtos.PieceOfNewsDTO(n.newsId, n.title, n.content, n.createdAt, n.creator.user.name) " +
            "from PieceOfNews n where n.relatedGroup.groupId = :groupId " +
            "order by n.createdAt desc"
    )
    List<PieceOfNewsDTO> getFlatListByRelatedGroupId(@Param("groupId") String groupId);

    @Query(value = "SELECT NEW de.groups.dtos.PieceOfNewsDTO(n.newsId, n.title, n.content, n.createdAt, n.creator.user.name, g.name, g.category) " +
            "from PieceOfNews n, Group g where g.groupId = n.relatedGroup.groupId " +
            "order by n.createdAt desc"
    )
    List<PieceOfNewsDTO> getAllAsFlatList();

    @Query(value = "SELECT count(n) from PieceOfNews n where n.creator.user.name = :userName")
    Integer getCountByUserName(@Param("userName") String userName);
}
