package de.groups.repositories;

import de.groups.models.Invitation;
import de.groups.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvitationRepository extends JpaRepository<Invitation, String> {
    List<Invitation> findByRelatedUser(User user);
}
