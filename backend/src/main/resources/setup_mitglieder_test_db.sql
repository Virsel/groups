DROP DATABASE IF EXISTS mitglieder_verwaltung_test_db;

DROP USER IF EXISTS mitglieder_admin;

CREATE USER mitglieder_admin WITH PASSWORD 'password123';

CREATE DATABASE mitglieder_verwaltung_test_db OWNER mitglieder_admin;

GRANT ALL PRIVILEGES ON DATABASE mitglieder_verwaltung_test_db TO mitglieder_admin;

