DROP DATABASE IF EXISTS mitglieder_verwaltung;

DROP DATABASE IF EXISTS mitglieder_verwaltung_test;

DROP USER IF EXISTS mitglieder_admin;

CREATE USER mitglieder_admin WITH PASSWORD 'password123';

CREATE DATABASE mitglieder_verwaltung OWNER mitglieder_admin;

GRANT ALL PRIVILEGES ON DATABASE mitglieder_verwaltung TO mitglieder_admin;

CREATE DATABASE mitglieder_verwaltung_test OWNER mitglieder_admin;

GRANT ALL PRIVILEGES ON DATABASE mitglieder_verwaltung_test TO mitglieder_admin;

connect mitglieder_verwaltung;

CREATE TYPE permission AS ENUM (
    'DELETE_GROUP',
    'INVITE_MEMBER',
    'EDIT_GROUP_ENTRY_REQUEST',
    'REMOVE_MEMBER',
    'CREATE_ROLE',
    'DELETE_ROLE',
    'ADD_ROLE_TO_MEMBER',
    'REMOVE_ROLE_FROM_MEMBER',
    'CREATE_PIECE_OF_NEWS',
    'DELETE_PIECE_OF_NEWS',
    'EDIT_PIECE_OF_NEWS',
    'DELETE_OWN_PIECE_OF_NEWS',
    'EDIT_OWN_PIECE_OF_NEWS'
);

