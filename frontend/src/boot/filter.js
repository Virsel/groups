import {boot} from 'quasar/wrappers'
import moment from 'moment'

export default boot(({app}) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$filter = {
    formatDate: function (value) {
      if (value) {
        return moment(String(value)).format('MM/DD/YYYY HH:mm')
      }
    }
  }
})

