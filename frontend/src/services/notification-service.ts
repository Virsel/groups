//Toast Service
import iziToast from 'izitoast'

let toasts: any

export const ToastService = {
  info: function (title: any, message: any, icon: any = '', position: any = 'bottomRight', t: any = 5000) {
    iziToast.show({
      class: 'toast',
      icon: icon,
      title: title,
      message: message,
      titleColor: '#fff',
      messageColor: '#fff',
      iconColor: '#fff',
      backgroundColor: '#0023ff',
      progressBarColor: '#bc7aff',
      position: position,
      transitionIn: 'fadeInDown',
      close: false,
      timeout: t,
      zindex: 99999,
    })
  },
  success: function (title: any, message: any, icon: any = '', position: any = 'bottomRight', t: any = 5000) {
    iziToast.show({
      class: 'toast',
      icon: icon,
      title: title,
      message: message,
      titleColor: '#fff',
      messageColor: '#fff',
      iconColor: '#fafafa',
      backgroundColor: '#21BA45',
      progressBarColor: '#fafafa',
      position: position,
      transitionIn: 'fadeInDown',
      close: false,
      timeout: t,
      zindex: 99999,
    })
  },
  error: function (title: any, message: any, icon: any = 'fas fa-meh', position: any = 'bottomRight', t: any = 5000) {
    iziToast.show({
      class: 'toast',
      icon: icon,
      title: title,
      message: message,
      titleColor: '#fff',
      messageColor: '#fff',
      iconColor: '#fafafa',
      backgroundColor: '#C10015',
      progressBarColor: '#fafafa',
      position: position,
      transitionIn: 'fadeInDown',
      close: false,
      timeout: t,
      zindex: 99999,
    })
  }
}
