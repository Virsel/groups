import {storeToRefs} from "pinia";
import {useUserDataStore} from "src/stores/user-data-store";
import {AxiosResponse} from "axios";
import {GetGroupListResp} from "src/api-client/models/group/responses/get-list";
import userClient from "src/api-client/clients/restricted/user-client";
import {GroupOfMember} from "src/models/DAOs/group-of-member";
import groupMemberClient from "src/api-client/clients/restricted/group-member-client";

export class GoupSelectOption {

}

export const UserDataService = {
  getUserGroups: async function (): Promise<GroupOfMember[]> {
    let result: GroupOfMember[] | null = null
    const fetchRes = await new Promise((resolve, reject) => userClient.getGroups().subscribe({
        next: (x: AxiosResponse<GetGroupListResp>) => {
          console.log('axios response...', x)
          storeToRefs(useUserDataStore()).userGroups.value = x.data.Data as GroupOfMember[]
          result = x.data.Data
          resolve(null)
        },
        error: (e) => {
          resolve(e)
        }
      })
    )

    // if no connection to api
    if (result == null) {
      console.log("no user groups were fetched")
      result = []
    }

    return result
  },

  leaveGroup(groupId: string) {
    groupMemberClient.leaveGroup(groupId).subscribe(
      next => {
        useUserDataStore().leaveGroup(groupId)
      }
    )
  }
}
