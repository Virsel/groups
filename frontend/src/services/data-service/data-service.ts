import groupClient from "src/api-client/clients/public/group-client";
import {AxiosResponse} from "axios";
import {GetGroupListResp} from "src/api-client/models/group/responses/get-list";
import {GroupCard} from "src/models/group-card";
import {GroupDetail} from "src/models/entities/group";
import {GetGroupResp} from "src/api-client/models/group/responses/get";

export const DataService = {
  getAllGroups: async function (): Promise<GroupCard[] | null> {
    let result: GroupCard[] | null = null
    const fetchRes = await new Promise((resolve, reject) => groupClient.getList().subscribe({
        next: (x: AxiosResponse<GetGroupListResp>) => {
          console.log('axios response...', x)
          let groups = x.data.Data;
          console.log('axios response...', groups)
          result = GroupCard.listFromGroups(groups)
          resolve(null)
        },
        error: (e) => {
          resolve(e)
        }
      })
    )

    if (result == null) {
      console.log("no groups were fetched")
    }

    return result
  },


  getGroupDetail: async function (group: string): Promise<GroupDetail | null> {
    let result: GroupDetail | null = null
    const fetchRes = await new Promise((resolve, reject) => groupClient.getDetail(group).subscribe({
        next: (x: AxiosResponse<GetGroupResp>) => {
          console.log('axios response...', x)
          result = x.data.Data
          resolve(null)
        },
        error: (e) => {
          resolve(e)
        }
      })
    )

    if (result == null) {
      console.log("no group detail were fetched")
    }

    return result
  },
}
