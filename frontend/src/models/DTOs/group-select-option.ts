import {Group} from "src/models/entities/group";


export class GroupSelectOption {
  Value: string | null = null;
  Label: string | null = null;


  constructor(GroupId: string | null, Name: string | null) {
    this.Value = GroupId;
    this.Label = Name;
  }

  static fromGroups(groups: Group[]) {
    let res = []
    for (const group of groups) {
      res.push(new GroupSelectOption(group.GroupId, group.Name))
    }
    return res
  }
}
