export class GroupEntryRequest {
  GroupEntryRequestId: string | null = null;
  RelatedGroupName: string | null = null;
  Status: GroupEntryRequestStatus | null = null;
  CreatedAt: Date | null = null;
}

export enum GroupEntryRequestStatus {
  IN_PROGRESS = 'in Bearbeitung',
  ACCEPTED = 'Angenommen',
  DENIED = 'Abgelehnt',
}



