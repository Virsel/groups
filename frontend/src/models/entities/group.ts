import {GroupMemberDAO} from "src/models/DAOs/group-member";
import {GroupDetailPieceOfNews} from "src/models/DAOs/group-detail-piece-of-news";

export class Group {
  GroupId: string | null = null;
  Name: string | null = null;
  Category: string | null = null;
  Description: string | null = null;
}

export class GroupDetail extends Group {
  FounderName: string | null = null;
  CreatedAt: Date | null = null;
  Members: GroupMemberDAO[] | null = null;
  News: GroupDetailPieceOfNews[] | null = null;
}
