import {GroupEntryRequest} from "src/models/entities/group-entry-request";

export class UserEntryRequest extends GroupEntryRequest{
  Issuer: string | null = null;
}
