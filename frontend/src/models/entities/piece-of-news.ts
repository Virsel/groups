export class PieceOfNewsDetail {
  NewsId: string | null = null;
  Title: string | null = null;
  Content: string | null = null;
  CreatedAt: Date | null = null;
  AuthorName: string | null = null;
  GroupName: string | null = null;
  Category: string | null = null;
}

