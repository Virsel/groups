import {GroupEntryRequest} from "src/models/entities/group-entry-request";

export class Invitation extends GroupEntryRequest{
  Issuer: string | null = null;
  RelatedUser: string | null = null;
}
