import {MemberRoleDAO} from "src/models/DAOs/group-member";

export class GroupOfMember {
  GroupId: string | null = null;
  GroupRepresantationId: string | null = null;
  Name: string | null = null;
  Category: string | null = null;
  Description: string | null = null;
  Roles: MemberRoleDAO[] | null = null;

  public static isModerator(group: GroupOfMember): boolean {
    return group.Roles?.find(x => x.Name === "Moderator" || "Admin") != null;
  }

  public static isAdmin(group: GroupOfMember): boolean {
    return group.Roles?.find(x => x.Name === "Admin") != null;
  }
}
