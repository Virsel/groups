export interface GroupMemberDAO {
  UserId: string
  GroupMemberId: string
  Name: string
  Roles: MemberRoleDAO[]
}

export interface GroupMemberDAOForRolesAdministration {
  Name: string
  GroupMemberId: string
  Roles: string[]
  NewRoles: string[]
}

export function groupMemberRolesChanged(member: GroupMemberDAOForRolesAdministration)
{
  if(member.Roles.length != member.NewRoles.length)
  {
    return true
  }

  var rolesChanged  = false
  for (const role of member.Roles) {
    rolesChanged = !member.NewRoles.includes(role);
    if(rolesChanged)
    {
      return rolesChanged
    }
  }
  return rolesChanged
}

export function toGroupMemberDAOForRolesAdministration(members: GroupMemberDAO[]): any
{
  var res: any  = []
  if(members == null)
  {
    return res
  }
  for (let member of members) {
    const entry: GroupMemberDAOForRolesAdministration = {
      Name: member.Name,
      GroupMemberId: member.GroupMemberId,
      Roles: member.Roles.map(x => x.Name),
      NewRoles: member.Roles.map(x => x.Name)
    }
    res.push(entry)
  }
  return res
}

export function GroupMemberHasNoteableRole(v: GroupMemberDAO): boolean {
  if (v.Roles.some(x => x.Name === "Admin" || x.Name === "Moderator")) {
    return true
  }
  return false
}

export function GetGroupMemberRoleName(v: GroupMemberDAO): string {
  if (v.Roles.some(x => x.Name === "Admin")) {
    return "Administrator"
  }
  if (v.Roles.some(x => x.Name === "Moderator")) {
    return "Moderator"
  }
  return ''
}

export interface MemberRoleDAO {
  MemberRoleId: string
  Name: string
  Permissions: string[]
}
