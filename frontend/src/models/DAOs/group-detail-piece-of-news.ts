export interface GroupDetailPieceOfNews {
  NewsId: string
  AuthorName: string
  Title: string
  Content: string
  CreatedAt: string
}
