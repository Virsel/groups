import {Group} from "src/models/entities/group";

export class GroupCard extends Group {
  Img: string | null = null;

  static listFromGroups(groups: Group[]): GroupCard[] {
    const res = []
    for (const group of groups) {
      const groupCard = group as GroupCard
      groupCard.Img = 'https://placeimg.com/500/300/nature?t=' + Math.random()
      res.push(groupCard)
    }
    return res
  }
}
