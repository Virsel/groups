const userRoutes = [
  {
    path: 'groups',
    name: 'user-groups',
    component: () => import('pages/user-specific/groups-specific/MemberGroupsOverview.vue')
  },
  {
    path: 'groups/create',
    name: 'user-group-create-view',
    component: () => import('pages/user-specific/groups-specific/GroupCreateView.vue')
  },
  {
    path: 'invitations',
    name: 'user-invitations',
    component: () => import('pages/user-specific/invitations-overview/Invitations')
  },
  {
    path: 'entry-requests',
    name: 'user-entry-requests',
    component: () => import('pages/user-specific/entry-requests-overview/EntryRequests')
  },
  {
    path: 'news',
    name: 'user-news',
    component: () => import('pages/user-specific/news-specific/MemberNewsListOverview.vue')
  },
  {
    path: 'news/create',
    name: 'user-news-create-view',
    component: () => import('pages/user-specific/news-specific/PieceOfNewsCreateView.vue')
  },
]

export default userRoutes
