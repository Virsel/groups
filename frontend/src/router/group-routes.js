const groupRoutes = [
  {
    path: ':group',
    name: 'group-detail',
    component: () => import('pages/groups-specific/GroupDetail.vue')
  },
  {
    path: ':group/administration',
    name: 'group-administration',
    component: () => import('pages/user-specific/group-admin-specific/GroupDetailAdministrationView.vue')
  },
]

export default groupRoutes
