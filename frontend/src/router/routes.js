import UserRoutes from "src/router/user-routes";
import GroupRoutes from "src/router/group-routes";

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {path: '', component: () => import('pages/Dashboard.vue')},
      {path: '/groups', name: 'all-groups', component: () => import('pages/groups-specific/GroupsOverview.vue')},
      {
        path: '/groups',
        component: () => import('pages/groups-specific/GroupDetailWrapper.vue'),
        children: GroupRoutes
      },
      {path: '/news', name: 'all-news', component: () => import('pages/news-list-overview/NewsListOverview.vue')},
      {
        path: '/user',
        component: () => import('pages/user-specific/MemberSpecificContentWrapper.vue'),
        children: UserRoutes
      },
      {path: '/profile', name: 'profile', component: () => import('pages/user-specific/profile/UserProfile.vue')},
      {path: '/login', name: 'login', component: () => import('pages/auth-specific/Login.vue')},
      {path: '/register', name: 'register', component: () => import('pages/auth-specific/Register.vue')},
      // Not completed yet
      // {path: '/Taskboard', component: () => import('pages/TaskBoard.vue')},
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  },
  {
    path: '/Maintenance',
    component: () => import('pages/Maintenance.vue')
  },
]

export default routes
