// src/stores/index.js
import {store} from 'quasar/wrappers'
import {createPinia} from 'pinia'
import {plugin as storePlugin} from 'src/helpers/pinia-plugins/persist/index'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store((/* { ssrContext } */) => {
  const pinia = createPinia()
  pinia.use(storePlugin)

  return pinia
})
