import {defineStore} from 'pinia'

export const useLayoutStore = defineStore('layout', {
  state: () => ({
    miniState: false,
    leftDrawerOpen: false
  }),
  getters: {},

  actions: {
    async toggleMiniState() {
      this.miniState = !this.miniState
    },
    async toggleLeftDrawer() {
      this.leftDrawerOpen = !this.leftDrawerOpen
    },
  },
  persistedState:
    {
      storage: localStorage,
      includePaths: ['miniState', 'leftDrawerOpen'],
    },
})

