import {defineStore} from 'pinia'
import {IUser} from 'src/models/entities/user'
import {GroupOfMember} from "src/models/DAOs/group-of-member";

export const useUserDataStore = defineStore('user-data', {
  state: () => ({
    user: null as IUser | null,
    userGroups: null as GroupOfMember[] | null
  }),
  getters: {
    getMember(): IUser | null {
      return this.user
    },
    loggedIn(): boolean {
      return this.user !== null
    },
    getMemberIdByGroup(): any {
      return (groupName: string) => {
        return this.userGroups?.find(x => x.Name === groupName)?.GroupRepresantationId
      }
    },
    isModeratorOfGroup(): any {
      return (groupName: string) => {
        var groupOfMember = this.userGroups?.find(x => x.Name === groupName)
        if (groupOfMember == undefined) {
          return false;
        }

        return GroupOfMember.isModerator(groupOfMember);
      }
    },
    groupsOfMemberWithAdminRole(): any {
        return  this.userGroups?.filter(x => GroupOfMember.isAdmin(x))
    },
    isAdminOfGroup(): any {
      return (groupName: string) => {
        var groupOfMember = this.userGroups?.find(x => x.Name === groupName)
        if (groupOfMember == undefined) {
          return false;
        }

        return GroupOfMember.isAdmin(groupOfMember);
      }
    },
  },

  actions: {
    async persistMember(user: IUser) {
      this.user = user
    },
    async clearMember() {
      this.user = null
    },
    async leaveGroup(groupId: string) {
      this.userGroups = this.userGroups!.filter(x => x.GroupId !== groupId);
    }
  },
  persistedState:
    {
      storage: localStorage,
      includePaths: ['user', 'userGroups'],
    },
})

