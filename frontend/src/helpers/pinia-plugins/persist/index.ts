export * from './plugin'
export * from './type'

import { createPersistedStatePlugin } from './plugin'

export const plugin = (context: any) => {
  const installPersistedStatePlugin = createPersistedStatePlugin()
  return installPersistedStatePlugin(context)
}
