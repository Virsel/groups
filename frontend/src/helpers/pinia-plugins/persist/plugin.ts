import { DebuggerEvent } from 'vue'
import type { PiniaPlugin, PiniaPluginContext, StateTree, SubscriptionCallback, } from 'pinia'
import type { CommonOptions, PluginOptions } from './type'

const shvl = require('shvl')

function defaultTo<T>(a: T | null | undefined, b: T) {
  return a != null ? a : b
}

function getOption<T, K extends keyof T>(
  fallback: T[K],
  key: K,
  options1: T,
  options2: T,
) {
  return defaultTo(
    defaultTo(options1[key], options2[key]),
    fallback,
  ) as Required<T>[K]
}

export function createPersistedStatePlugin<S extends StateTree = StateTree>(
  options?: PluginOptions<S>,
): PiniaPlugin {
  const pluginOptions = options || ({} as PluginOptions<StateTree>)

  function plugin(context: PiniaPluginContext) {
    // normalize
    const settings = (function () {
      try {
        return context.options.persistedState || {}
      } catch {
        return {}
      }
    })()

    function setPluginForSetting(setting: any) {
      if (getOption(true, 'persist', setting, pluginOptions) === false) return
      const key = getOption(context.store.$id, 'key', setting, {})
      const overwrite = getOption(false, 'overwrite', setting, pluginOptions)
      const storage = getOption(
        (function () {
          try {
            return window.localStorage
          } catch {
            return {
              getItem: function () {
              },
              setItem: function () {
              },
              removeItem: function () {
              },
            }
          }
        })(),
        'storage',
        setting,
        pluginOptions,
      )
      const filter = getOption(
        function () {
          return true
        },
        'filter',
        setting,
        pluginOptions,
      )
      const serialize = getOption(
        JSON.stringify,
        'serialize',
        setting,
        pluginOptions,
      )
      const deserialize = getOption(
        JSON.parse,
        'deserialize',
        setting,
        pluginOptions,
      )
      const migrate = getOption(
        function <T>(_: T) {
          return _
        },
        'migrate',
        setting,
        {},
      )
      const merge = getOption(
        function (state: any, savedState: any) {
          return savedState
        },
        'merge',
        setting,
        {},
      )

      if (process.env.NODE_ENV !== 'production') {
        if (setting.assertStorage === void 0) {
          setting.assertStorage = function (
            storage: Required<CommonOptions<S>>['storage'],
          ) {
            const uniqueKey = '@@'
            const result = storage.setItem(uniqueKey, '1')
            const removeItem = function () {
              storage.removeItem(uniqueKey)
            }
            if (result instanceof Promise) {
              result.then(removeItem)
            } else {
              removeItem()
            }
          }
        }
        setting.assertStorage(storage)
      }

      // hydrate
      // initialize custom properties
      let resolveIsReady: any
      const isReadyPromise = new Promise<void>(function (resolve) {
        resolveIsReady = resolve
      })
      let pendingCount = 0
      context.store.$persistedState = {
        isReady: function () {
          return isReadyPromise
        },
        pending: false,
      }

      function patchOrOverwrite(state: any) {
        (setting.beforeHydrate || function () {
        })(context.store.$state)
        const merged = merge(context.store.$state, state)
        if (overwrite) {
          context.store.$state = merged
        } else {
          context.store.$patch(merged)
        }
        resolveIsReady()
      }

      function parse(value: any) {
        if (value != null) {
          const state = deserialize(value)
          const migrateState = migrate(state)

          if (migrateState instanceof Promise) {
            migrateState.then(patchOrOverwrite)
          } else {
            patchOrOverwrite(migrateState)
          }
        } else {
          resolveIsReady()
        }
      }

      try {
        const value = storage.getItem(key)
        console.log('store persist initialization, node env:', process.env.NODE_ENV)
        if (value instanceof Promise) {
          value.then(parse)
        } else {
          parse(value)
        }
      } catch (error) {
        if (process.env.NODE_ENV !== 'production') console.warn(error)
        resolveIsReady!()
      }

      // persist
      const callback: SubscriptionCallback<S> = function (mutation, state) {
        if (filter(mutation, state) === false) return

        let globalfilter = false
        try {
          if (mutation.events as DebuggerEvent) {
            const target = (mutation.events as DebuggerEvent).target
            const newValue = (mutation.events as DebuggerEvent).newValue
            console.log('filter mutation event', target, typeof target)
            if (!(newValue instanceof Promise)) {
              console.log('filter mutation event for map', target, state)
              globalfilter = true
            }
          } else {
            const events = mutation.events as DebuggerEvent[]
            console.log('filter mutation events', events)
          }
        } catch (e) {
          console.log('filter mutation events failed', e)
        }
        if (globalfilter === false) return

        if (Array.isArray(setting.includePaths)) {
          state = setting.includePaths.reduce(function (partialState: any, path: any) {
            return shvl.set(
              partialState,
              path,
              shvl.get(state as Record<string, unknown>, path, void 0),
              void 0,
            )
          }, {} as any)
        }
        if (Array.isArray(setting.excludePaths)) {
          state = deserialize(serialize(state))
          setting.excludePaths.forEach(function (path: any) {
            return shvl.set(state, path, void 0, void 0)
          }, {})
        }

        const value = serialize(state)
        const result = storage.setItem(key, value)
        if (result instanceof Promise) {
          ++pendingCount
          context.store.$persistedState.pending = pendingCount !== 0
          result
            .catch(function () {
            })
            .finally(function () {
              --pendingCount
              context.store.$persistedState.pending = pendingCount !== 0
            })
        }
      }
      context.store.$subscribe(callback)
    }

    // dont init persistency if not specified
    if(Object.keys(settings).length === 0){
      return
    }
    if (Array.isArray(settings)) {
      for (const setting of settings) {
        setPluginForSetting(setting)
      }
    } else {
      setPluginForSetting(settings)
    }
  }

  return plugin
}
