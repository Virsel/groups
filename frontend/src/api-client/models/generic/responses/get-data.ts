import { BaseResp } from 'src/api-client/models/base-resp'

export interface GetDataResp<T> extends BaseResp {
  Data: T
}
