import {BaseResp} from 'src/api-client/models/base-resp'
import {GroupOfMember} from "src/models/DAOs/group-of-member";

export interface GetGroupListResp extends BaseResp {
  Data: GroupOfMember[]
}
