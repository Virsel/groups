import {BaseResp} from 'src/api-client/models/base-resp'
import {GroupDetail} from "src/models/entities/group";

export interface GetGroupResp extends BaseResp {
  Data: GroupDetail
}
