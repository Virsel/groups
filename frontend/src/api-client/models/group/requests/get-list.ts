// get
export class GetGroupListReq {
  Member: string | undefined

  toUrlQuery(){
    return `?member=${this.Member}`
  }
}
