// add
export class CreateGroupReq {
  Name: string;
  Category: string;
  Description: string;

  constructor(name: string, category: string, description: string) {
    this.Name = name;
    this.Category = category;
    this.Description = description;
  }
}
