import {PieceOfNewsDetail} from "src/models/entities/piece-of-news";

export class GetPieceOfNewsListResp {
  Data: PieceOfNewsDetail[] = []
}
