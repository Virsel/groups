import { BaseResp } from 'src/api-client/models/base-resp'
import {PieceOfNewsDetail} from "src/models/entities/piece-of-news";

export interface IGetPieceOfNewsResp extends BaseResp {
  Data: PieceOfNewsDetail
}
