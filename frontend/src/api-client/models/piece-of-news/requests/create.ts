export class CreatePieceOfNewsReq {
  GroupMemberId: string
  Title: string;
  Content: string;

  constructor(groupMemberId: string, title: string, content: string) {
    this.GroupMemberId = groupMemberId;
    this.Title = title;
    this.Content = content;
  }
}

