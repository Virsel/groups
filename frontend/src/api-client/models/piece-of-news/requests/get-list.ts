export class GetPieceOfNewsListReq {
  Group: string | undefined
  Member: string | undefined

  toUrlQuery(){
    return `?member=${this.Member}&group=${this.Group}`
  }
}
