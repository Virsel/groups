export interface BaseResp {
  Messages: string[]
  Errors: string[]
}
