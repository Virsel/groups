import {UserEntryRequest} from "src/models/entities/user-entry-request";

export class GetUserEntryRequestListResp {
  Data: UserEntryRequest[] = []
}
