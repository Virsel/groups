export class CreateUserEntryRequestReq {
  RelatedGroupName: string;
  IssuerName: string;


  constructor(RelatedGroup: string, Issuer: string) {
    this.RelatedGroupName = RelatedGroup;
    this.IssuerName = Issuer;
  }
}

