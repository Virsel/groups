
export class GetUserEntryRequestListReq {
  RelatedGroup: string

  constructor(RelatedGroup: string) {
    this.RelatedGroup = RelatedGroup;
  }

  toUrlQuery(){
    return `?relatedGroup=${this.RelatedGroup}`
  }
}
