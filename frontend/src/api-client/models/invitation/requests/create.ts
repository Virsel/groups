export class CreateInvitationReq {
  RelatedGroup: string;
  RelatedUser: string;
  Issuer: string;


  constructor(RelatedGroup: string, RelatedUser: string, Issuer: string) {
    this.RelatedGroup = RelatedGroup;
    this.RelatedUser = RelatedUser;
    this.Issuer = Issuer;
  }
}

