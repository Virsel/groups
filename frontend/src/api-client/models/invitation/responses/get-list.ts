import {Invitation} from "src/models/entities/invitation";

export class GetInvitationListResp {
  Data: Invitation[] = []
}
