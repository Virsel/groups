import {useUserDataStore} from "src/stores/user-data-store";

export const addUsernameOnRequestFulfilled = (config: any) => {
  const token = useUserDataStore().user?.Username
  console.log("username:", token)
  if (token) {
    console.log("access token:", token)
    config.headers!['x-access-token'] = token
  }
  return config
}
