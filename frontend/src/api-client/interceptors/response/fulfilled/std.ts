import {ToastService} from 'src/services/notification-service'

export const stdOnResponseFulfilled = (response: any) => {
  // all 2xx/3xx responses will end here
  if (response.data.Errors) {
    for (const err of response.data.Errors) {
      ToastService.error('', err)
    }
    Promise.resolve()
  }
  console.log("fulfilled resp data:", response.data);
  if (response.data.Messages) {
    console.log("fulfilled resp data 2:", response.data);
    for (const message of response.data.Messages) {
      ToastService.success('', message)
    }
    Promise.resolve()
  }
  return response
}
