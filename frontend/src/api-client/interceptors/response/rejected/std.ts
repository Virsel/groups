import {ToastService} from 'src/services/notification-service'

export const stdOnResponseRejected = (error: any) => {
  try {
    const errMsg = error.response.data
    console.log("Error antwort...", error.response)
    if (error.response.status === 401) {
      ToastService.error('', 'Du bist nicht authentifiziert')
    } else if (error.response.status === 403) {
      ToastService.error('', 'Du bist nicht autorisiert')
    } else if (errMsg !== undefined && typeof errMsg === 'string' && errMsg !== '') {
      ToastService.error('', errMsg)
    } else {
      throw null
    }
  } catch (e) {
    ToastService.error('', 'Es ist ein unbekannter Fehler aufgetreten')
  }

  return Promise.reject(error)
}
