

export const refreshAuthTokenOnResponseRejected = async (err: any) => {
    const originalConfig = err.config;
    if (originalConfig.url !== "/auth/signin" && err.response) {
      // Access Token was expired
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;
        try {
          // webLibraryClient.post("/auth/refreshtoken", {
          //   refreshToken: TokenService.getLocalRefreshToken(),
          // }).subscribe(rs => {
          //   const { accessToken } = rs.data;
          //   store.dispatch('auth/refreshToken', accessToken);
          //   TokenService.updateLocalAccessToken(accessToken);
          //   return webLibraryClient(originalConfig);
          // })
        } catch (_error) {
          return Promise.reject(_error);
        }
      }
    }
    return Promise.reject(err);
  }
