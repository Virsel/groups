const ApiBaseUrl = "http://localhost:8080/api"
const GroupBaseUrl = ApiBaseUrl + "/groups/"
const PieceOfNewsBaseUrl = ApiBaseUrl + "/news/"
const UserEntryRequestBaseUrl = ApiBaseUrl + "/user-entry-requests/"
const InvitationBaseUrl = ApiBaseUrl + "/invitations/"
const UserBaseUrl = ApiBaseUrl + "/user/"
const GroupMemberBaseUrl = ApiBaseUrl + "/group-member/"
const GroupModeratorBaseUrl = ApiBaseUrl + "/group-moderator/"
const GroupAdminBaseUrl = ApiBaseUrl + "/group-admin/"


module.exports = {
  GroupBaseUrl: GroupBaseUrl,
  PieceOfNewsBaseUrl: PieceOfNewsBaseUrl,
  UserEntryRequestBaseUrl: UserEntryRequestBaseUrl,
  InvitationBaseUrl: InvitationBaseUrl,
  ApiBaseUrl: ApiBaseUrl,
  GroupMemberBaseUrl: GroupMemberBaseUrl,
  GroupModeratorBaseUrl: GroupModeratorBaseUrl,
  GroupAdminBaseUrl: GroupAdminBaseUrl,
  UserBaseUrl: UserBaseUrl
};


