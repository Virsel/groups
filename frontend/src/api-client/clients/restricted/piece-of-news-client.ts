import Axios, {AxiosObservable} from 'axios-observable'
import {PieceOfNewsBaseUrl} from 'src/api-client/clients/constants'
import {restrictedBaseClient} from 'src/api-client/clients/restricted/restricted-base-client'
import {CreatePieceOfNewsReq} from "src/api-client/models/piece-of-news/requests/create";

export const restrictedPieceOfNewsClient = Axios.create({
  baseURL: PieceOfNewsBaseUrl

})

restrictedPieceOfNewsClient.interceptors.response = restrictedBaseClient.interceptors.response
restrictedPieceOfNewsClient.interceptors.request = restrictedBaseClient.interceptors.request

export default {
  create(payload: CreatePieceOfNewsReq): AxiosObservable<any> {
    return restrictedPieceOfNewsClient.post('', payload)
  },
  delete(newsId: string): AxiosObservable<any> {
    return restrictedPieceOfNewsClient.delete(`${newsId}`)
  },
}
