import Axios, { AxiosObservable } from 'axios-observable'
import { GroupBaseUrl } from 'src/api-client/clients/constants'
import { restrictedBaseClient } from 'src/api-client/clients/restricted/restricted-base-client'
import {CreateGroupReq} from "src/api-client/models/group/requests/create";
import {GetGroupListReq} from "src/api-client/models/group/requests/get-list";
import {GetGroupListResp} from "src/api-client/models/group/responses/get-list";

export const restrictedGroupClient = Axios.create({
  baseURL: GroupBaseUrl
})

restrictedGroupClient.interceptors.response = restrictedBaseClient.interceptors.response
restrictedGroupClient.interceptors.request = restrictedBaseClient.interceptors.request

export default {
  create(payload: CreateGroupReq): AxiosObservable<any>{
    return restrictedGroupClient.post('', payload)
  },
}
