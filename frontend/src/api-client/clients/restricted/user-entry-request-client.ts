import Axios, {AxiosObservable} from 'axios-observable'
import {UserEntryRequestBaseUrl} from 'src/api-client/clients/constants'
import {restrictedBaseClient} from 'src/api-client/clients/restricted/restricted-base-client'
import {CreateUserEntryRequestReq} from "src/api-client/models/user-entry-request/requests/create";
import {GetDataResp} from "src/api-client/models/generic/responses/get-data";
import {GroupEntryRequest} from "src/models/entities/group-entry-request";

export const userEntryRequestClient = Axios.create({
  baseURL: UserEntryRequestBaseUrl
})

userEntryRequestClient.interceptors.response = restrictedBaseClient.interceptors.response
userEntryRequestClient.interceptors.request = restrictedBaseClient.interceptors.request

export default {
  create(payload: CreateUserEntryRequestReq): AxiosObservable<any> {
    return userEntryRequestClient.post('', payload)
  },
  get(): AxiosObservable<GetDataResp<GroupEntryRequest[]>> {
    return userEntryRequestClient.get('')
  },
}
