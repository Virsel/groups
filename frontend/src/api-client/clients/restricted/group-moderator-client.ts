import Axios, {AxiosObservable} from 'axios-observable'
import {GroupModeratorBaseUrl} from 'src/api-client/clients/constants'
import {restrictedBaseClient} from 'src/api-client/clients/restricted/restricted-base-client'
import {BaseResp} from "src/api-client/models/base-resp";

export const groupModeratorClient = Axios.create({
  baseURL: GroupModeratorBaseUrl
})

groupModeratorClient.interceptors.response = restrictedBaseClient.interceptors.response
groupModeratorClient.interceptors.request = restrictedBaseClient.interceptors.request

export default {
  deleteNews(newsId: string): AxiosObservable<BaseResp> {
    return groupModeratorClient.delete(`news/${newsId}`)
  },
}
