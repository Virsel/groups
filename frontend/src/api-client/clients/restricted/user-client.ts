import Axios, {AxiosObservable} from 'axios-observable'
import {UserBaseUrl} from 'src/api-client/clients/constants'
import {restrictedBaseClient} from 'src/api-client/clients/restricted/restricted-base-client'
import {GetInvitationListResp} from "src/api-client/models/invitation/responses/get-list";
import {GetGroupListResp} from "src/api-client/models/group/responses/get-list";
import {GetDataResp} from "src/api-client/models/generic/responses/get-data";
import {PieceOfNewsDetail} from "src/models/entities/piece-of-news";

export const userClient = Axios.create({
  baseURL: UserBaseUrl
})

userClient.interceptors.response = restrictedBaseClient.interceptors.response
userClient.interceptors.request = restrictedBaseClient.interceptors.request

export default {
  getInvitations(): AxiosObservable<GetInvitationListResp> {
    return userClient.get("invitations")
  },
  getGroups(): AxiosObservable<GetGroupListResp> {
    return userClient.get("groups")
  },
  getNews(): AxiosObservable<GetDataResp<PieceOfNewsDetail[]>> {
    return userClient.get("news")
  },
}
