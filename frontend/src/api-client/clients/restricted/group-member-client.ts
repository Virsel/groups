import Axios, {AxiosObservable} from 'axios-observable'
import {GroupMemberBaseUrl} from 'src/api-client/clients/constants'
import {restrictedBaseClient} from 'src/api-client/clients/restricted/restricted-base-client'
import {BaseResp} from "src/api-client/models/base-resp";

export const groupMemberClient = Axios.create({
  baseURL: GroupMemberBaseUrl
})

groupMemberClient.interceptors.response = restrictedBaseClient.interceptors.response
groupMemberClient.interceptors.request = restrictedBaseClient.interceptors.request

export default {
  leaveGroup(groupMemberId: string): AxiosObservable<BaseResp> {
    return groupMemberClient.delete(`leave/${groupMemberId}`)
  },
}
