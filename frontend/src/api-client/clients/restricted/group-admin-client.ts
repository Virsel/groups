import Axios, {AxiosObservable} from 'axios-observable'
import {GroupAdminBaseUrl} from 'src/api-client/clients/constants'
import {restrictedBaseClient} from 'src/api-client/clients/restricted/restricted-base-client'
import {BaseResp} from "src/api-client/models/base-resp";
import {GetDataResp} from "src/api-client/models/generic/responses/get-data";
import {ForeignUserDAO} from "src/models/DAOs/foreign-user";

export const groupAdminClient = Axios.create({
  baseURL: GroupAdminBaseUrl
})

groupAdminClient.interceptors.response = restrictedBaseClient.interceptors.response
groupAdminClient.interceptors.request = restrictedBaseClient.interceptors.request

export default {
  deleteGroup(groupId: string): AxiosObservable<BaseResp> {
    return groupAdminClient.delete(`groups/${groupId}`)
  },
  getUsersForInvitation(groupId: string): AxiosObservable<GetDataResp<ForeignUserDAO[]>> {
    return groupAdminClient.get(`groups/${groupId}/users-for-invitation`)
  },
  deleteGroupMember(groupMemberId: string): AxiosObservable<BaseResp> {
    return groupAdminClient.delete(`members/${groupMemberId}`)
  },
  inviteUser(data: {UserId: string, IssuerId: string}): AxiosObservable<BaseResp> {
    return groupAdminClient.post(`invitations`, data)
  },
  editMemberRoles(groupMemberId: string, roles: string[]): AxiosObservable<BaseResp> {
    return groupAdminClient.post(`members/${groupMemberId}/roles`, roles)
  },
}
