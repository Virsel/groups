import Axios from 'axios-observable'
import { ApiBaseUrl } from 'src/api-client/clients/constants'
import { addUsernameOnRequestFulfilled } from 'src/api-client/interceptors/request/fulfilled/add-username'
import { stdOnRequestRejected } from 'src/api-client/interceptors/request/rejected/std'
import { stdOnResponseFulfilled } from 'src/api-client/interceptors/response/fulfilled/std'
import { stdOnResponseRejected } from 'src/api-client/interceptors/response/rejected/std'

export const restrictedBaseClient = Axios.create({
  baseURL: ApiBaseUrl
})

restrictedBaseClient.interceptors.request.use(addUsernameOnRequestFulfilled, stdOnRequestRejected)
restrictedBaseClient.interceptors.response.use(stdOnResponseFulfilled, stdOnResponseRejected)



