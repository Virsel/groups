import Axios, {AxiosObservable} from 'axios-observable'
import {PieceOfNewsBaseUrl} from 'src/api-client/clients/constants'
import {GetPieceOfNewsListResp} from "src/api-client/models/piece-of-news/responses/get-list";
import {publicBaseClient} from "src/api-client/clients/public/public-base-client";

export const pieceOfNewsClient = Axios.create({
  baseURL: PieceOfNewsBaseUrl

})

pieceOfNewsClient.interceptors.response = publicBaseClient.interceptors.response
pieceOfNewsClient.interceptors.request = publicBaseClient.interceptors.request

export default {
  getList(): AxiosObservable<GetPieceOfNewsListResp> {
    return pieceOfNewsClient.get("")
  },
}
