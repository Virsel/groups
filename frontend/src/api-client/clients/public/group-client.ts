import Axios, {AxiosObservable} from 'axios-observable'
import {GroupBaseUrl} from 'src/api-client/clients/constants'
import {GetGroupListResp} from "src/api-client/models/group/responses/get-list";
import {GetGroupResp} from "src/api-client/models/group/responses/get";
import {publicBaseClient} from "src/api-client/clients/public/public-base-client";

export const groupClient = Axios.create({
  baseURL: GroupBaseUrl
})

groupClient.interceptors.response = publicBaseClient.interceptors.response
groupClient.interceptors.request = publicBaseClient.interceptors.request

export default {
  getList(): AxiosObservable<GetGroupListResp> {
    return groupClient.get("")
  },
  getDetail(group: string): AxiosObservable<GetGroupResp> {
    return groupClient.get(group)
  },
}
