import Axios from 'axios-observable'
import { stdOnRequestRejected } from 'src/api-client/interceptors/request/rejected/std'
import { stdOnResponseFulfilled } from 'src/api-client/interceptors/response/fulfilled/std'
import { stdOnResponseRejected } from 'src/api-client/interceptors/response/rejected/std'
import { stdOnRequestFulfilled } from 'src/api-client/interceptors/request/fulfilled/std'
import { ApiBaseUrl } from 'src/api-client/clients/constants'

export const publicBaseClient = Axios.create({
  baseURL: ApiBaseUrl
})

publicBaseClient.interceptors.request.use(stdOnRequestFulfilled, stdOnRequestRejected)
publicBaseClient.interceptors.response.use(stdOnResponseFulfilled, stdOnResponseRejected)
